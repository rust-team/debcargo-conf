Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: wezterm-bidi
Upstream-Contact: Wez Furlong <wez@wezfurlong.org>
Source: https://github.com/wez/wezterm

Files: *
Copyright: 2022-Present Wez Furlong
License: MIT

Files: debian/*
Copyright:
 2024 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2024 Ananthu C V <weepingclown@disroot.org>
License: MIT

Files: debian/missing-sources/data/DerivedBidiClass.txt
Copyright: 1991-2024 Unicode, Inc.
License: Unicode-License-V3
Comment: Used for generating src/bidi_class.rs file, unable to use unicode-data package
	 as upstream generation script  uses unicode 14 file and debian has unicode 15

Files: debian/missing-sources/generate/src/* debian/missing-sources/generate/Cargo.toml
Copyright: 2022-Present Wez Furlong
License: MIT
Comment: Used for generating src/bidi_*.rs files from the data files

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: Unicode-License-V3
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of data files and any associated documentation (the "Data Files") or
 software and any associated documentation (the "Software") to deal in the
 Data Files or Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, and/or sell
 copies of the Data Files or Software, and to permit persons to whom the
 Data Files or Software are furnished to do so, provided that either (a)
 this copyright and permission notice appear with all copies of the Data
 Files or Software, or (b) this copyright and permission notice appear in
 associated Documentation.
 .
 THE DATA FILES AND SOFTWARE ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 THIRD PARTY RIGHTS.
 .
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS INCLUDED IN THIS NOTICE
 BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES,
 OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THE DATA
 FILES OR SOFTWARE.
 .
 Except as contained in this notice, the name of a copyright holder shall
 not be used in advertising or otherwise to promote the sale, use or other
 dealings in these Data Files or Software without prior written
 authorization of the copyright holder.