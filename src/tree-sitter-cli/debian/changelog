rust-tree-sitter-cli (0.20.8-6) unstable; urgency=medium

  * Team upload.
  * Package tree-sitter-cli 0.20.8 from crates.io using debcargo 2.7.0
  * Rearrange patches, move all regex related patching to regex-syntax-0.8.patch
  * Relax dependency on which.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 13 Oct 2024 17:46:00 +0000

rust-tree-sitter-cli (0.20.8-5) unstable; urgency=medium

  * Team upload.
  * Package tree-sitter-cli 0.20.8 from crates.io using debcargo 2.6.1
  * Bump toml dependency to 0.8.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 30 Jan 2024 18:11:29 +0000

rust-tree-sitter-cli (0.20.8-4) unstable; urgency=medium

  * Team upload.
  * Package tree-sitter-cli 0.20.8 from crates.io using debcargo 2.6.0
  * Add patch for new version of regex-syntax.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 25 Oct 2023 00:02:29 +0000

rust-tree-sitter-cli (0.20.8-3) unstable; urgency=medium

  * Team upload.
  * Package tree-sitter-cli 0.20.8 from crates.io using debcargo 2.6.0
  * Rearrange patches for easier testing.
  * Remove upper limit from indexmap dependency.
  * Bump regex-syntax dependency to 0.7.4
  * Remove upper limit from ctor dev-dependency
  * Bump toml dependency to 0.7

 -- Peter Michael Green <plugwash@debian.org>  Thu, 05 Oct 2023 04:01:15 +0000

rust-tree-sitter-cli (0.20.8-2) unstable; urgency=medium

  * Team upload.
  * Package tree-sitter-cli 0.20.8 from crates.io using debcargo 2.6.0

 -- Peter Michael Green <plugwash@debian.org>  Thu, 27 Jul 2023 14:57:11 +0000

rust-tree-sitter-cli (0.20.8-1) unstable; urgency=medium

  * Team upload.
  * Package tree-sitter-cli 0.20.8 from crates.io using debcargo 2.6.0
  * Drop min-tree-sitter-dep.patch, upstream has bumped the dependency.
  * Update gate-tests-atomic-u64.patch and gate-tests-atomic-u64.patch
    for new upstream
  * Stop patching tiny_http and webbrowser dependencies, upstream is now
    in line with Debian.
  * Bump dirs dependency to 5.0

 -- Peter Michael Green <plugwash@debian.org>  Thu, 27 Jul 2023 05:17:51 +0000

rust-tree-sitter-cli (0.20.7-3) unstable; urgency=medium

  * Team upload.
  * Gate test code that requires 64-bit atomics behind
    #[cfg(target_has_atomic = "64")]
  * Stop overriding debian/tests/control, edit Cargo.toml instead.
  * Add patch to deal with char signedness issue in test.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 22 Feb 2023 01:30:17 +0000

rust-tree-sitter-cli (0.20.7-2) experimental; urgency=medium

  * Package tree-sitter-cli 0.20.7 from crates.io using debcargo 2.6.0
  * Add emscripten to Recommends for "tree-sitter build-wasm"
  * Update relax-deps.patch to allow building against rust-webbrowser 0.8

 -- James McCoy <jamessan@debian.org>  Mon, 20 Feb 2023 20:05:47 -0500

rust-tree-sitter-cli (0.20.7-1) experimental; urgency=medium

  * Package tree-sitter-cli 0.20.7 from crates.io using debcargo 2.6.0
    (Closes: #1029745)
  * New patches
    + relax-deps: Allow use of pretty-assertions 1
    + skip-tests-using-fixtures: Add "#[ignore]" attribute to tests which
      rely on unpackaged fixtures
    + writable-scratch-dir: Use CARGO_TARGET_DIR or CARGO_TARGET_TMPDIR as the
      SCRATCH_DIR for tests, so autopkgtests have a writable directory to use
  * Disable benchmarks since they rely on unpackaged fixtures

 -- James McCoy <jamessan@debian.org>  Thu, 09 Feb 2023 23:32:51 -0500
