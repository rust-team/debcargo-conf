Description: Cherry-pick upstream upgrade to rkyv 0.8, adapted for Debian
Origin: https://github.com/ParkMyCar/compact_str/commit/39c782746b9b6832b7727accc3fa3d88fb136a35
From: Parker Timmerman <parker@parkertimmerman.com>
Date: Sun, 29 Dec 2024 16:31:23 -0500
Subject: [PATCH] deps: upgrade to `rkyv v0.8` (#409)

* start, upgrade rkyv to v0.8

* upgrades rkyv to v0.8
* makes necessary changes to the impl

* update tests
---
 compact_str/Cargo.toml           |  4 +--
 compact_str/src/features/rkyv.rs | 46 ++++++++++++++++++--------------
 2 files changed, 28 insertions(+), 22 deletions(-)

--- a/Cargo.toml
+++ b/Cargo.toml
@@ -98,4 +98,4 @@
 [dependencies.rkyv]
-version = "0.7"
-features = ["size_32"]
+version = "0.8"
+#features = ["size_32"]
 optional = true
@@ -150,8 +150,8 @@
 [dev-dependencies.rkyv]
-version = "0.7"
-features = [
-    "alloc",
-    "size_32",
-]
-default-features = false
+version = "0.8"
+#features = [
+#    "alloc",
+#    "size_32",
+#]
+#default-features = false
 
--- a/src/features/rkyv.rs
+++ b/src/features/rkyv.rs
@@ -10,3 +10,3 @@
     DeserializeUnsized,
-    Fallible,
+    rancor::{Fallible, Source}, Place,
     Serialize,
@@ -22,4 +22,4 @@
     #[inline]
-    unsafe fn resolve(&self, pos: usize, resolver: Self::Resolver, out: *mut Self::Archived) {
-        ArchivedString::resolve_from_str(self.as_str(), pos, resolver, out);
+    fn resolve(&self, resolver: Self::Resolver, out: Place<Self::Archived>) {
+        ArchivedString::resolve_from_str(self.as_str(), resolver, out);
     }
@@ -30,2 +30,3 @@
     str: SerializeUnsized<S>,
+    S::Error: Source,
 {
@@ -65,3 +66,4 @@
 
-    use rkyv::Deserialize;
+    use rkyv::string::ArchivedString;
+    use rkyv::{rancor, Archive};
     use test_strategy::proptest;
@@ -75,9 +77,9 @@
 
-        let bytes_compact = rkyv::to_bytes::<_, 32>(&CompactString::from(VALUE)).unwrap();
-        let bytes_control = rkyv::to_bytes::<_, 32>(&String::from(VALUE)).unwrap();
+        let bytes_compact = rkyv::to_bytes::<rancor::Error>(&CompactString::from(VALUE)).unwrap();
+        let bytes_control = rkyv::to_bytes::<rancor::Error>(&String::from(VALUE)).unwrap();
         assert_eq!(&*bytes_compact, &*bytes_control);
 
-        let archived = unsafe { rkyv::archived_root::<CompactString>(&bytes_compact) };
-        let compact: CompactString = archived.deserialize(&mut rkyv::Infallible).unwrap();
-        let control: String = archived.deserialize(&mut rkyv::Infallible).unwrap();
+        let archived = rkyv::access::<ArchivedString, rancor::Error>(&bytes_compact).unwrap();
+        let compact = rkyv::deserialize::<CompactString, rancor::Error>(archived).unwrap();
+        let control = rkyv::deserialize::<String, rancor::Error>(archived).unwrap();
         assert_eq!(archived, VALUE);
@@ -86,5 +88,5 @@
 
-        let archived = unsafe { rkyv::archived_root::<String>(&bytes_compact) };
-        let compact: CompactString = archived.deserialize(&mut rkyv::Infallible).unwrap();
-        let control: String = archived.deserialize(&mut rkyv::Infallible).unwrap();
+        let archived = rkyv::access::<ArchivedString, rancor::Error>(&bytes_compact).unwrap();
+        let compact = rkyv::deserialize::<CompactString, rancor::Error>(archived).unwrap();
+        let control = rkyv::deserialize::<String, rancor::Error>(archived).unwrap();
         assert_eq!(archived, VALUE);
@@ -97,9 +99,11 @@
     fn proptest_roundtrip(s: String) {
-        let bytes_compact = rkyv::to_bytes::<_, 32>(&CompactString::from(&s)).unwrap();
-        let bytes_control = rkyv::to_bytes::<_, 32>(&s).unwrap();
+        let bytes_compact = rkyv::to_bytes::<rancor::Error>(&CompactString::from(&s)).unwrap();
+        let bytes_control = rkyv::to_bytes::<rancor::Error>(&s).unwrap();
         assert_eq!(&*bytes_compact, &*bytes_control);
 
-        let archived = unsafe { rkyv::archived_root::<CompactString>(&bytes_compact) };
-        let compact: CompactString = archived.deserialize(&mut rkyv::Infallible).unwrap();
-        let control: String = archived.deserialize(&mut rkyv::Infallible).unwrap();
+        let archived =
+            rkyv::access::<<CompactString as Archive>::Archived, rancor::Error>(&bytes_compact)
+                .unwrap();
+        let compact = rkyv::deserialize::<CompactString, rancor::Error>(archived).unwrap();
+        let control = rkyv::deserialize::<String, rancor::Error>(archived).unwrap();
         assert_eq!(archived, &s);
@@ -108,5 +112,6 @@
 
-        let archived = unsafe { rkyv::archived_root::<String>(&bytes_compact) };
-        let compact: CompactString = archived.deserialize(&mut rkyv::Infallible).unwrap();
-        let control: String = archived.deserialize(&mut rkyv::Infallible).unwrap();
+        let archived =
+            rkyv::access::<<String as Archive>::Archived, rancor::Error>(&bytes_compact).unwrap();
+        let compact = rkyv::deserialize::<CompactString, rancor::Error>(archived).unwrap();
+        let control = rkyv::deserialize::<String, rancor::Error>(archived).unwrap();
         assert_eq!(archived, &s);
