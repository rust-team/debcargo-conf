rust-ripasso (0.7.0-1) unstable; urgency=medium

  * Team upload.
  * Package ripasso 0.7.0 from crates.io using debcargo 2.7.7
  * Upgrade to config 0.15
  * Upgrade to sequoia-gpg-agent 0.5

 -- Blair Noctis <ncts@debian.org>  Tue, 04 Mar 2025 15:33:05 +0000

rust-ripasso (0.6.5-6) unstable; urgency=medium

  * Package ripasso 0.6.5 from crates.io using debcargo 2.7.6

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Tue, 04 Feb 2025 14:00:54 +0100

rust-ripasso (0.6.5-5) unstable; urgency=medium

  * Team upload.
  * Package ripasso 0.6.5 from crates.io using debcargo 2.7.0
  * Relax dependencies on base64 and git2.

 -- Peter Michael Green <plugwash@debian.org>  Fri, 11 Oct 2024 09:05:21 +0000

rust-ripasso (0.6.5-4) unstable; urgency=medium

  * Team upload.
  * Package ripasso 0.6.5 from crates.io using debcargo 2.7.0
  * update to base64 0.22
  * update to git2 0.19

 -- Fabian Grünbichler <debian@fabian.gruenbichler.email>  Sat, 05 Oct 2024 16:41:54 +0200

rust-ripasso (0.6.5-3) unstable; urgency=medium

  * Package ripasso 0.6.5 from crates.io using debcargo 2.6.1
  * Add patch to handle move of gnupg module from sequoia-ipc to
    sequoia-gpg-agent.

  [ Blair Noctis ]
  * Team upload.
  * Disable criterion
  * Ease a test check where output is right but a little excessive
  * Drop fix-testdata-location-in-bench.patch as benches are disabled
  * Remove totp-rs from relax-deps.patch

 -- Peter Michael Green <plugwash@debian.org>  Thu, 01 Aug 2024 16:03:58 +0000

rust-ripasso (0.6.5-2) unstable; urgency=medium

  * Team upload.
  * Package ripasso 0.6.5 from crates.io using debcargo 2.6.1
  * Bump sequoia-ipc dependency to 0.34

 -- Peter Michael Green <plugwash@debian.org>  Thu, 21 Mar 2024 12:58:54 +0000

rust-ripasso (0.6.5-1) unstable; urgency=medium

  * Package ripasso 0.6.5 from crates.io using debcargo 2.6.1
  * Bump sequoia-ipc dependency to 0.33

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Mon, 04 Mar 2024 17:36:05 +0100

rust-ripasso (0.6.4-4) unstable; urgency=medium

  * Team upload.
  * Package ripasso 0.6.4 from crates.io using debcargo 2.6.1
  * Bump sequoia-ipc dependency to 0.31

 -- James McCoy <jamessan@debian.org>  Thu, 21 Dec 2023 10:00:47 -0500

rust-ripasso (0.6.4-3) unstable; urgency=medium

  * Team upload.
  * Package ripasso 0.6.4 from crates.io using debcargo 2.6.1
  * Bump git2 dependency to 0.18

 -- Peter Michael Green <plugwash@debian.org>  Sun, 03 Dec 2023 17:36:23 +0000

rust-ripasso (0.6.4-2) unstable; urgency=medium

  * Team upload.
  * Package ripasso 0.6.4 from crates.io using debcargo 2.6.0
  * Attempt to fix permission denied errors on debci.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 02 Nov 2023 01:41:51 +0000

rust-ripasso (0.6.4-1) unstable; urgency=medium

  * Team upload.
  * Package ripasso 0.6.4 from crates.io using debcargo 2.6.0
  * Update relax-deps.patch for new upstream and current situation in Debian.
  * Drop disable-criterion.patch, no longer needed.
  * Fix finding test data in criterion_benchmark_load_4_passwords when run
    with an explicit target.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 31 Oct 2023 20:21:06 +0000

rust-ripasso (0.6.1-2) unstable; urgency=medium

  * Team upload.
  * Package ripasso 0.6.1 from crates.io using debcargo 2.6.0
  * Bump git2 dependency to 0.26.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 12 Jan 2023 19:25:52 +0000

rust-ripasso (0.6.1-1) unstable; urgency=medium

  * Package ripasso 0.6.1 from crates.io using debcargo 2.6.0

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Wed, 21 Dec 2022 13:09:46 +0100
