rust-sequoia-octopus-librnp (1.10.0-5) unstable; urgency=medium

  * Package sequoia-octopus-librnp 1.10.0 from crates.io using debcargo 2.7.6
  * Upgrade to newer versions of sequoia-gpg-agent and sequoia-net

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Sun, 09 Feb 2025 12:14:51 +0100

rust-sequoia-octopus-librnp (1.10.0-4) unstable; urgency=medium

  * Package sequoia-octopus-librnp 1.10.0 from crates.io using debcargo 2.7.4
  * Enable build with sequoia-wot 0.13

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 25 Nov 2024 22:42:45 -0500

rust-sequoia-octopus-librnp (1.10.0-3) unstable; urgency=medium

  * Package sequoia-octopus-librnp 1.10.0 from crates.io using debcargo 2.7.1
  * debian/patches: add dep3 headers. Separate disable-crypo-cng.patch
    from drop-windows.patch.

 -- Holger Levsen <holger@debian.org>  Tue, 22 Oct 2024 20:18:14 +0200

rust-sequoia-octopus-librnp (1.10.0-2) unstable; urgency=medium

  * Package sequoia-octopus-librnp 1.10.0 from crates.io using debcargo 2.7.0
    - Relax relax-deps.patch to allow build against sequoia-policy-config 0.7.

 -- Holger Levsen <holger@debian.org>  Sat, 12 Oct 2024 00:22:46 +0200

rust-sequoia-octopus-librnp (1.10.0-1) unstable; urgency=medium

  * Package sequoia-octopus-librnp 1.10.0 from crates.io using debcargo 2.6.1
    - Closes: #1078248.
  * Update debian/tests/rnp_symbols as needed.

 -- Holger Levsen <holger@debian.org>  Wed, 28 Aug 2024 10:58:27 +0200

rust-sequoia-octopus-librnp (1.9.0-2) unstable; urgency=medium

  * Package sequoia-octopus-librnp 1.9.0 from crates.io using debcargo 2.6.1
    - Closes: #1076760.
  * Disable autopkgtests for ppc64el to get the package into trixie.

 -- Holger Levsen <holger@debian.org>  Tue, 06 Aug 2024 18:07:16 +0900

rust-sequoia-octopus-librnp (1.9.0-1) unstable; urgency=medium

  * Package sequoia-octopus-librnp 1.9.0 from crates.io using debcargo 2.6.1
  * Refresh patches.

 -- Holger Levsen <holger@debian.org>  Sun, 28 Jul 2024 16:54:05 +0900

rust-sequoia-octopus-librnp (1.8.1-4) unstable; urgency=medium

  * libsequoia-octopus-librnp.preinst: fix copy and paste error, sigh.
    Closes: #1069686.

 -- Holger Levsen <holger@debian.org>  Tue, 23 Apr 2024 11:09:23 +0200

rust-sequoia-octopus-librnp (1.8.1-3) unstable; urgency=medium

  * libsequoia-octopus-librnp.preinst:
    - fix typo "thunderbid". Closes: #1069594.
    - also install diversion on upgrades. Closes: #1069593.

 -- Holger Levsen <holger@debian.org>  Mon, 22 Apr 2024 12:02:29 +0200

rust-sequoia-octopus-librnp (1.8.1-2) unstable; urgency=medium

  * dpkg-divert /usr/lib/thunderbird/librnp.so in preinst and postrm.
    Closes: #1041832.

 -- Holger Levsen <holger@debian.org>  Thu, 28 Mar 2024 00:26:49 +0100

rust-sequoia-octopus-librnp (1.8.1-1) unstable; urgency=medium

  * Team upload.
  * Package sequoia-octopus-librnp 1.8.1 from crates.io using debcargo 2.6.1

  [ Daniel Kahn Gillmor ]
  * Package sequoia-octopus-librnp 1.4.1 from crates.io using debcargo 2.5.0
  * Recommend: zenity | kdialog for feedback during potentially lengthy
    keyring loads

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Wed, 20 Mar 2024 20:25:31 +0100

rust-sequoia-octopus-librnp (1.4.1-1) experimental; urgency=medium

  * Package sequoia-octopus-librnp 1.4.1 from crates.io using debcargo 2.5.0
  * Because this is a cdylib crate, and debcargo and dh-cargo don't know
    how to handle that yet (see #1017681 and #1017682), we diverge pretty
    widely from rust-team common practice.
  * Closes: #1005740

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 19 Aug 2022 00:01:31 -0400
