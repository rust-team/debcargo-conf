rust-pgp (0.15.0-2) unstable; urgency=medium

  * Package pgp 0.15.0 from crates.io using debcargo 2.7.6
  * Drop backward-compat test more completely

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sun, 02 Feb 2025 23:04:33 -0500

rust-pgp (0.15.0-1) unstable; urgency=medium

  * Package pgp 0.15.0 from crates.io using debcargo 2.7.6
  * Refresh patches (dropping several adopted upstream)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 30 Jan 2025 13:31:54 -0500

rust-pgp (0.14.2-3) unstable; urgency=medium

  * Package pgp 0.14.2 from crates.io using debcargo 2.7.6
  * Permit build with bitfield 0.17

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 10 Jan 2025 11:48:14 -0500

rust-pgp (0.14.2-2) unstable; urgency=medium

  * Package pgp 0.14.2 from crates.io using debcargo 2.7.5
  * avoid an x448 test while x448 is still underdeveloped

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 05 Dec 2024 23:58:18 -0500

rust-pgp (0.14.2-1) unstable; urgency=medium

  * Package pgp 0.14.2 from crates.io using debcargo 2.7.5

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 05 Dec 2024 11:21:08 -0500

rust-pgp (0.14.0-3) unstable; urgency=medium

  * Package pgp 0.14.0 from crates.io using debcargo 2.7.2
  * drop benches and profile/gperftools feature to reduce time/compute
    spent in CI.

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sat, 02 Nov 2024 11:38:38 +0000

rust-pgp (0.14.0-2) unstable; urgency=medium

  * Package pgp 0.14.0 from crates.io using debcargo 2.7.2
  * avoid writing to the source tree during tests (see
    https://github.com/rpgp/rpgp/issues/430)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 29 Oct 2024 21:43:40 -0400

rust-pgp (0.14.0-1) unstable; urgency=medium

  * Package pgp 0.14.0 from crates.io using debcargo 2.7.1

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 23 Oct 2024 17:24:42 -0400

rust-pgp (0.13.2-3) unstable; urgency=medium

  * Package pgp 0.13.2 from crates.io using debcargo 2.7.1
  * Use base64 0.22, aligned with upstream

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sat, 19 Oct 2024 17:55:21 -0400

rust-pgp (0.13.2-2) unstable; urgency=medium

  * Package pgp 0.13.2 from crates.io using debcargo 2.6.1
  * Tests: Stop writing to the current working directory (see
    https://github.com/rpgp/rpgp/issues/403)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 09 Sep 2024 12:44:41 -0400

rust-pgp (0.13.2-1) unstable; urgency=medium

  * Package pgp 0.13.2 from crates.io using debcargo 2.6.1

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 30 Aug 2024 00:32:41 -0400
