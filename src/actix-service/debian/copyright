Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: actix-service
Upstream-Contact:
 Nikolay Kim <fafhrd91@gmail.com>
 Rob Ede <robjtede@icloud.com>
 fakeshadow <24548779@qq.com>
Source: https://github.com/actix/actix-net

Files: *
Copyright:
 2018-2020 Nikolay Kim <fafhrd91@gmail.com>
 2020-2021 Rob Ede <robjtede@icloud.com>
 2020-2021 fakeshadow <24548779@qq.com>
 2017-2021 Actix Team
License: MIT or Apache-2.0

Files: debian/*
Copyright:
 2019-2025 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2019-2025 Wolfgang Silbermayr <wolfgang@silbermayr.at>
 2025 kpcyrd <git@rxv.cc>
License: MIT or Apache-2.0

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
