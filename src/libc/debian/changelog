rust-libc (0.2.169-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.169 from crates.io using debcargo 2.7.5
  * Rearrange patches for easier testing.
  * Fix defintion of struct stat on i386.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 28 Dec 2024 20:35:00 +0000

rust-libc (0.2.168-2) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.168 from crates.io using debcargo 2.7.5
  * Fix build on i386.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 16 Dec 2024 08:21:19 +0000

rust-libc (0.2.168-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.168 from crates.io using debcargo 2.7.5
  * Drop add-missing-stat-fields.patch, included upstream.
  * Update patches for new upstream.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 15 Dec 2024 15:35:22 +0000

rust-libc (0.2.161-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.161 from crates.io using debcargo 2.7.0
  * Add check of gnu_time64_abi cfg to conditions for using
    __clock_nanosleep_time64 so it is not incorrectly used on i386.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 05 Nov 2024 09:13:45 +0000

rust-libc (0.2.159-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.159 from crates.io using debcargo 2.7.1
  * Update time64-04-use-time64-symbols.patch with moved ioctl, ctime_r

 -- Blair Noctis <ncts@debian.org>  Fri, 11 Oct 2024 09:26:23 +0000

rust-libc (0.2.155-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.155 from crates.io using debcargo 2.6.1
  * Add breaks on old version of librust-nix-dev.
  * Update patches for new upstream.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 23 May 2024 22:08:08 +0000

rust-libc (0.2.153-6) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.153 from crates.io using debcargo 2.6.1
  * Add upstream patch to fix missing stat fields.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 28 Apr 2024 10:14:42 +0000

rust-libc (0.2.153-5) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.153 from crates.io using debcargo 2.6.1
  * Really fix stat related definitions on i386.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 24 Apr 2024 00:19:10 +0000

rust-libc (0.2.153-3) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.153 from crates.io using debcargo 2.6.1
  * Fix stat related definitions on i386.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 23 Apr 2024 13:38:21 +0000

rust-libc (0.2.153-2) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.153 from crates.io using debcargo 2.6.1
  * Add patches for 64-bit time_t, based on upstream pull request.
  * Add depends on version of dpkg-dev that enables time64.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 21 Apr 2024 19:33:48 +0000

rust-libc (0.2.153-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.153 from crates.io using debcargo 2.6.1

 -- Peter Michael Green <plugwash@debian.org>  Tue, 12 Mar 2024 13:32:38 +0000

rust-libc (0.2.152-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.152 from crates.io using debcargo 2.6.1

 -- Peter Michael Green <plugwash@debian.org>  Sun, 14 Jan 2024 23:05:56 +0000

rust-libc (0.2.150-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.150 from crates.io using debcargo 2.6.0

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 27 Nov 2023 22:09:12 +0100

rust-libc (0.2.149-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.149 from crates.io using debcargo 2.6.0

 -- Peter Michael Green <plugwash@debian.org>  Sun, 08 Oct 2023 17:53:57 +0000

rust-libc (0.2.147-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.147 from crates.io using debcargo 2.6.0

 -- Peter Michael Green <plugwash@debian.org>  Sun, 27 Aug 2023 17:34:30 +0000

rust-libc (0.2.146-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.146 from crates.io using debcargo 2.6.0

 -- Michael Tokarev <mjt@tls.msk.ru>  Tue, 13 Jun 2023 09:36:53 +0300

rust-libc (0.2.139-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.139 from crates.io using debcargo 2.6.0 (Closes: #1027330)

 -- Peter Michael Green <plugwash@debian.org>  Fri, 30 Dec 2022 22:12:34 +0000

rust-libc (0.2.138-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.138 from crates.io using debcargo 2.6.0 (Closes: #1025525)

 -- Peter Michael Green <plugwash@debian.org>  Tue, 06 Dec 2022 13:53:43 +0000

rust-libc (0.2.137-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.137 from crates.io using debcargo 2.5.0

 -- Peter Michael Green <plugwash@debian.org>  Sat, 29 Oct 2022 07:10:03 +0000

rust-libc (0.2.132-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.132 from crates.io using debcargo 2.5.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 22 Aug 2022 09:34:58 +0200

rust-libc (0.2.126-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.126 from crates.io using debcargo 2.5.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 01 Jun 2022 15:54:11 +0200

rust-libc (0.2.125-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.125 from crates.io using debcargo 2.5.0

 -- James McCoy <jamessan@debian.org>  Mon, 16 May 2022 20:53:27 -0400

rust-libc (0.2.103-1) unstable; urgency=medium

  * Package libc 0.2.103 from crates.io using debcargo 2.4.4-alpha.0
  * Disable autopkgtest for "const-extern-fn" feature which requires a nightly
    comiler

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Sun, 03 Oct 2021 14:03:37 +0200

rust-libc (0.2.80-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.80 from crates.io using debcargo 2.4.3

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 04 Nov 2020 10:04:28 +0100

rust-libc (0.2.73-1) unstable; urgency=medium

  * Package libc 0.2.73 from crates.io using debcargo 2.4.3

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Thu, 23 Jul 2020 18:17:01 +0200

rust-libc (0.2.68-2) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.68 from crates.io using debcargo 2.4.2
  * Disable autopkgtest for "rustc-dep-of-std" featureset, it requires
    a nightly compiler.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 12 Apr 2020 19:55:56 +0000

rust-libc (0.2.68-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.68 from crates.io using debcargo 2.4.2

 -- Andrej Shadura <andrewsh@debian.org>  Tue, 07 Apr 2020 11:53:34 +0200

rust-libc (0.2.66-1) unstable; urgency=medium

  * Package libc 0.2.66 from crates.io using debcargo 2.4.2

 -- Ximin Luo <infinity0@debian.org>  Mon, 06 Jan 2020 16:30:31 +0000

rust-libc (0.2.65-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.65 from crates.io using debcargo 2.4.0

 -- Andrej Shadura <andrewsh@debian.org>  Tue, 12 Nov 2019 15:13:58 +0100

rust-libc (0.2.62-1) unstable; urgency=medium

  * Team upload.
  * Package libc 0.2.62 from crates.io using debcargo 2.2.10

 -- nicoo <nicoo@debian.org>  Wed, 28 Aug 2019 14:05:41 +0200

rust-libc (0.2.59-1) unstable; urgency=medium

  * Package libc 0.2.59 from crates.io using debcargo 2.3.1-alpha.0

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Thu, 11 Jul 2019 08:12:13 +0200

rust-libc (0.2.55-1) unstable; urgency=medium

  * Team upload. (Closes: #929394)
  * Package libc 0.2.55 from crates.io using debcargo 2.2.10

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 22 May 2019 17:23:21 -0400

rust-libc (0.2.48-1) unstable; urgency=medium

  * Package libc 0.2.48 from crates.io using debcargo 2.2.9

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Tue, 05 Feb 2019 17:32:43 +0100

rust-libc (0.2.47-1) unstable; urgency=medium

  * Package libc 0.2.47 from crates.io using debcargo 2.2.9

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Mon, 21 Jan 2019 13:54:20 +0100

rust-libc (0.2.46-1) unstable; urgency=medium

  * Package libc 0.2.46 from crates.io using debcargo 2.2.9

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Fri, 11 Jan 2019 08:53:48 +0100

rust-libc (0.2.45-1) unstable; urgency=medium

  * Package libc 0.2.45 from crates.io using debcargo 2.2.9

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Wed, 26 Dec 2018 15:03:24 -0800

rust-libc (0.2.43-1) unstable; urgency=medium

  * Package libc 0.2.43 from crates.io using debcargo 2.2.6
  * Fix the lintian warning "description-too-long"

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 26 Aug 2018 11:49:44 +0200

rust-libc (0.2.42-1) unstable; urgency=medium

  * Package libc 0.2.42 from crates.io using debcargo 2.1.1

 -- Ximin Luo <infinity0@debian.org>  Fri, 22 Jun 2018 00:36:47 -0700
