rust-toml (0.8.19-1) unstable; urgency=medium

  * Team upload.
  * Package toml 0.8.19 from crates.io using debcargo 2.6.1
    (Closes: #1069653, #1074511)
  * Drop relax-deps.diff, the version of indexmap upstream wants now matches
    the version Debian has.
  * Update patches for new upstream.
  * Disable tests that use snapbox, Debian's version of snapbox is too
    different from the version upstream wants to make patching sustainable.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 08 Sep 2024 08:29:45 +0000

rust-toml (0.8.8-2) unstable; urgency=medium

  * Team upload.
  * Package toml 0.8.8 from crates.io using debcargo 2.6.1 (Closes: #1059104. #1053955)

 -- Peter Michael Green <plugwash@debian.org>  Tue, 30 Jan 2024 16:08:01 +0000

rust-toml (0.8.8-1) experimental; urgency=medium

  * Team upload.
  * Package toml 0.8.8 from crates.io using debcargo 2.6.0
  * Update patches for new upstream.
  * Remove dev-dependency on toml-test-data.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 15 Nov 2023 00:10:52 +0000

rust-toml (0.7.6-1) unstable; urgency=medium

  * Team upload.
  * Package toml 0.7.6 from crates.io using debcargo 2.6.0 (Closes: #1042495)
  * Relax indexmap dependency to >= 1.9.1
  * Relax snapbox dependency to 0.4.8
  * Remove dev-dependency on toml-test-harness and disable tests that depend on
    it so the rest of the tests can run.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 26 Aug 2023 12:51:47 +0000

rust-toml (0.5.11-1) unstable; urgency=medium

  * Team upload.
  * Package toml 0.5.11 from crates.io using debcargo 2.6.0 (Closes: #1030799)

 -- Peter Michael Green <plugwash@debian.org>  Tue, 07 Feb 2023 22:07:35 +0000

rust-toml (0.5.10-1) unstable; urgency=medium

  * Team upload.
  * Package toml 0.5.10 from crates.io using debcargo 2.6.0 (Closes: #1027327, #1027328)

 -- Peter Michael Green <plugwash@debian.org>  Fri, 30 Dec 2022 19:02:10 +0000

rust-toml (0.5.9-1) unstable; urgency=medium

  * Team upload.
  * Package toml 0.5.9 from crates.io using debcargo 2.6.0 (Closes: #1024381)
  * Collapse features

 -- Blair Noctis <n@sail.ng>  Sun, 20 Nov 2022 08:19:51 +0000

rust-toml (0.5.8-1) unstable; urgency=medium

  * Team upload.
  * Package toml 0.5.8 from crates.io using debcargo 2.4.4

 -- Ximin Luo <infinity0@debian.org>  Sat, 23 Oct 2021 19:29:52 +0100

rust-toml (0.5.5-1) unstable; urgency=medium

  * Team upload.
  * Package toml 0.5.5 from crates.io using debcargo 2.4.0

 -- Ximin Luo <infinity0@debian.org>  Thu, 28 Nov 2019 00:40:54 +0000

rust-toml (0.5.1-2) unstable; urgency=medium

  * Team upload.
  * Package toml 0.5.1 from crates.io using debcargo 2.2.10
  * Source upload for migration

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 06 Aug 2019 18:13:11 +0200

rust-toml (0.5.1-1) unstable; urgency=medium

  * Package toml 0.5.1 from crates.io using debcargo 2.2.10

 -- kpcyrd <git@rxv.cc>  Sun, 14 Jul 2019 10:20:17 +0200

rust-toml (0.4.10-1) unstable; urgency=medium

  * Team upload.
  * Package toml 0.4.10 from crates.io using debcargo 2.2.9

 -- Ximin Luo <infinity0@debian.org>  Thu, 27 Dec 2018 03:33:06 -0800

rust-toml (0.4.8-1) unstable; urgency=medium

  * Team upload.
  * Package toml 0.4.8 from crates.io using debcargo 2.2.7

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Thu, 04 Oct 2018 11:49:40 +0200

rust-toml (0.4.6-1) unstable; urgency=medium

  * Package toml 0.4.6 from crates.io using debcargo 2.2.1

  [ kpcyrd <git@rxv.cc> ]
  * Package toml 0.4.6 from crates.io using debcargo 2.1.4

 -- Ximin Luo <infinity0@debian.org>  Sat, 07 Jul 2018 14:04:02 -0700
