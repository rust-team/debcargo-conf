rust-xh (0.24.0-1) unstable; urgency=medium

  * Package xh 0.24.0 from crates.io using debcargo 2.7.7
  * Enable digest auth with replaced dependency
  * Enable network interface binding
  * Disable shell completion generation for Nushell due to lacking dependency
  of librust-clap-complete-nushell-dev
  * Improve description
  * Alias xh(1) to xhs(1)

 -- Blair Noctis <ncts@debian.org>  Tue, 18 Feb 2025 14:59:01 +0000

rust-xh (0.23.1-3) unstable; urgency=medium

  * Make man page generation reproducible, Closes: #1092917
  * Link xh to xhs, Closes: #1093425

 -- Blair Noctis <ncts@debian.org>  Mon, 20 Jan 2025 17:24:00 +0000

rust-xh (0.23.1-2) unstable; urgency=medium

  * Cherry-pick upstream commit for runtime --generate option for completions
  and man page, and generate them using it
  * Package xh 0.23.1 from crates.io using debcargo 2.7.6

 -- Blair Noctis <ncts@debian.org>  Sat, 11 Jan 2025 16:32:45 +0000

rust-xh (0.23.1-1) unstable; urgency=medium

  * Package xh 0.23.1 from crates.io using debcargo 2.7.6
  Closes: #1051320 (Thank you, debcargo, the first line is obviously yours only)
  * Disable HTTP Auth Digest scheme due to unavailable dep digest_auth
  * Disable interface binding due to unavailable dep network-interface and
  reqwest 0.12
  * Replace functionaliy provided by unavailable dep mime2ext with a naive impl
  * Disable webpki-roots for reqwest

 -- Blair Noctis <ncts@debian.org>  Thu, 09 Jan 2025 20:42:52 +0000
