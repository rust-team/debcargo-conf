Source: rust-coreutils
Section: utils
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 24),
 dh-sequence-bash-completion,
 cargo:native,
 rustc:native,
 libstd-rust-dev,
 librust-clap-4-dev, librust-clap-complete-4-dev,
 librust-clap-mangen-dev,
 librust-backtrace-dev,
 librust-half-2+default-dev (>= 2.4),
 librust-lazy-static-1+default-dev (>= 1.3-~~),
 librust-same-file-1.0.6+default-dev | librust-same-file-1.0.5+default-dev | librust-same-file-1.0.4+default-dev,
 librust-textwrap-dev (>= 0.16.0), librust-cc-dev,
 librust-quick-error-dev (>= 2.0), librust-unix-socket-dev, librust-getopts-dev,
 librust-filetime-dev (>= 0.2.17),
 librust-walkdir-dev (>= 2.3.2), librust-xattr-dev (>= 1.0.0), librust-number-prefix-dev,
 librust-ansi-width-dev,
# librust-num-prime-dev,
 librust-bincode-dev, librust-serde-big-array-dev,
 librust-rust-ini-dev (>= 0.18.0),
 librust-glob-dev (>= 0.3.1), librust-chrono-dev (>= 0.4.31), librust-onig-dev (>= 6.3),
 librust-chrono-tz-dev,
 librust-blake2-rfc-dev, librust-digest-dev (>= 0.10.5), librust-hex-dev, librust-md5-dev,
 librust-sha1-dev (>= 0.10.1), librust-sha2-dev, librust-sha3-dev,
 librust-atty-dev, librust-uutils-term-grid-dev (>= 0.6.0), librust-termsize-dev,
 librust-tempfile-dev, librust-nix-0.29-dev, librust-fs-extra-dev,
 librust-itertools-dev (>= 0.10.5), librust-bit-set-dev, librust-dunce-dev (>= 1.0.3),
 librust-data-encoding-dev, librust-platform-info-dev (>= 1.0.2),
 librust-num-traits-dev (>= 0.2.14), librust-smallvec-dev (>= 1.9.0),
 librust-rand-pcg-dev, librust-thiserror-dev, librust-lazy-static-dev,
 librust-byteorder-dev, librust-hostname-dev (>= 0.3.1), librust-cpp-build-dev,
 librust-fnv-dev, librust-wild-dev, librust-cpp-dev, librust-thread-local-dev,
 librust-selinux-dev (>= 0.3.0), librust-selinux-sys-dev (>= 0.6.7-2), librust-conv-dev, librust-pretty-assertions-dev (>= 1),
 librust-time-dev (>= 0.3.9), librust-unindent-dev (>= 0.2.1), librust-rlimit-dev (>= 0.8.3),
 librust-data-encoding-macro-dev, librust-dns-lookup-dev, librust-os-display-dev,
 librust-z85-dev, librust-fts-sys-dev, librust-exacl-dev, librust-byte-unit-dev,
 librust-gcd-dev, librust-signal-hook-dev (>= 0.3.15), librust-paste-dev, librust-quickcheck-dev,
 librust-crossterm-dev (>= 0.25.0), librust-bigdecimal-dev, librust-binary-heap-plus-dev,
 librust-ouroboros-dev, librust-memmap2-dev (>= 0.5), librust-retain-mut-dev (>= 0.1.7),
 librust-strum-dev, librust-utf-8-dev, librust-phf-dev, librust-phf-codegen-dev,
 librust-zip-dev, librust-blake2b-simd-dev,
 librust-hex-literal-dev, librust-lscolors-dev (>= 0.14.0-4), librust-blake3-dev,
 librust-file-diff-dev, librust-ctrlc-dev (>= 3.4.0), librust-flate2-dev,
 librust-bstr-dev (>= 1.5.0), librust-md-5-dev, librust-bytecount-dev (>= 0.6.3),
 librust-regex-automata-dev, librust-notify-dev,
 librust-procfs-dev, librust-terminal-size-dev (>= 0.2.1),
 librust-indicatif-dev (>= 0.17.3),
 librust-fundu-dev, librust-parse-datetime-dev,
 librust-is-terminal-dev, librust-sm3-dev, librust-self-cell-dev,
 librust-uutils-term-grid-dev, librust-rstest-dev,
 librust-backtrace-sys-dev, python3-sphinx, librust-utmp-classic-dev
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Sylvestre Ledru <sylvestre@debian.org>
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/coreutils]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/coreutils
Homepage: https://github.com/uutils/coreutils
Rules-Requires-Root: no

Package: rust-coreutils
Architecture: any
Multi-Arch: allowed
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 ${cargo:Depends}
Recommends:
 ${cargo:Recommends}
Suggests:
 ${cargo:Suggests}
Provides:
 ${cargo:Provides}
Built-Using: ${cargo:Built-Using}
XB-X-Cargo-Built-Using: ${cargo:X-Cargo-Built-Using}
Description: Universal coreutils utils, written in Rust
 This packages replaces the GNU coreutils package written in C.
 It should be a drop-in replacement but:
  * Some options have NOT been implemented,
  * Might have important bugs,
  * Might be slower,
  * Output of the binaries might be slightly different.
