rust-debcargo (2.7.8-1) unstable; urgency=medium

  * Package debcargo 2.7.8 from crates.io using debcargo 2.7.7
  * add `debcargo deb-dependencies` (Closes: #1051501)
  * split generated Build-Depends into -Arch and -Indep
  * allow overriding Breaks, Replaces and Conflicts
  * fix version ranges with <!nocheck>
  * add manpages (Closes: #971633)

 -- Fabian Grünbichler <debian@fabian.gruenbichler.email>  Fri, 07 Mar 2025 09:46:24 +0100

rust-debcargo (2.7.7-1) unstable; urgency=medium

  * Package debcargo 2.7.7 from crates.io using debcargo 2.7.6
  * Update to cargo 0.85
  * Version ranges are now encoded using bounds instead of a series of
    alternative dependencies (Closes: #967954)

 -- Fabian Grünbichler <debian@fabian.gruenbichler.email>  Fri, 14 Feb 2025 23:04:09 +0100

rust-debcargo (2.7.6-4) unstable; urgency=medium

  * Team upload.
  * Use parallel=4 in DEB_BUILD_OPTIONS with dh_auto_install for armhf.

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Tue, 14 Jan 2025 21:36:40 +0000

rust-debcargo (2.7.6-3) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.7.6 from crates.io using debcargo 2.7.6
  * Relax dependency on env-logger.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 01 Jan 2025 12:27:58 +0000

rust-debcargo (2.7.6-2) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.7.6 from crates.io using debcargo 2.7.5
  * Bump env_logger dependency to v0.11

 -- NoisyCoil <noisycoil@tutanota.com>  Tue, 31 Dec 2024 13:26:51 +0100

rust-debcargo (2.7.6-1) unstable; urgency=medium

  * Package debcargo 2.7.6 from crates.io using debcargo 2.7.5

 -- Fabian Grünbichler <debian@fabian.gruenbichler.email>  Sun, 29 Dec 2024 19:34:13 +0100

rust-debcargo (2.7.5-1) unstable; urgency=medium

  * Package debcargo 2.7.5 from crates.io using debcargo 2.7.5

 -- Fabian Grünbichler <debian@fabian.gruenbichler.email>  Sun, 24 Nov 2024 16:27:54 +0100

rust-debcargo (2.7.4-1) unstable; urgency=medium

  * Package debcargo 2.7.4 from crates.io using debcargo 2.7.2

 -- Fabian Grünbichler <debian@fabian.gruenbichler.email>  Mon, 11 Nov 2024 21:37:03 +0100

rust-debcargo (2.7.2-1) unstable; urgency=medium

  * Package debcargo 2.7.2 from crates.io using debcargo 2.7.1

 -- Fabian Grünbichler <debian@fabian.gruenbichler.email>  Tue, 22 Oct 2024 19:29:39 +0200

rust-debcargo (2.7.1-2) unstable; urgency=medium

  * Package debcargo 2.7.1 from crates.io using debcargo 2.7.0
  * Update git2 to 0.19 / libgit2 to 1.8

 -- Fabian Grünbichler <debian@fabian.gruenbichler.email>  Sat, 05 Oct 2024 16:42:49 +0200

rust-debcargo (2.7.1-1) unstable; urgency=medium

  * Package debcargo 2.7.1 from crates.io using debcargo 2.7.0
  * Update to cargo 0.81, itertools 0.13
  * Fix generated d/watch for pre-release versions

 -- Fabian Grünbichler <debian@fabian.gruenbichler.email>  Fri, 04 Oct 2024 19:15:12 +0200

rust-debcargo (2.7.0-1) unstable; urgency=medium

  * Package debcargo 2.7.0 from crates.io using debcargo 2.7.0
  * update to cargo 0.76
  * bump the standard version to 4.7.0 for generated source packages
  * no longer warn about uncollapsed packages
  * Also generate a 'Static-Built-Using' in addition to the 'X-Cargo-Built-Using'
  * Add rustc to autopkgtest Depends of generated source packages
  * Switch dh compat to 13 for generated source packages

 -- Fabian Grünbichler <debian@fabian.gruenbichler.email>  Fri, 20 Sep 2024 15:08:30 +0200

rust-debcargo (2.6.1-6) unstable; urgency=medium

  * Team upload.
  * Update to cargo 0.76

 -- Fabian Grünbichler <debian@fabian.gruenbichler.email>  Fri, 12 Jul 2024 20:19:43 +0200

rust-debcargo (2.6.1-5) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.6.1 from crates.io using debcargo 2.6.1
  * Relax dependencies on itertools and toml.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 30 Jan 2024 16:30:42 +0000

rust-debcargo (2.6.1-4) unstable; urgency=medium

  * Team upload.
  * Fix sparse access of crates.io

 -- Fabian Grünbichler <debian@fabian.gruenbichler.email>  Tue, 19 Dec 2023 09:26:56 +0100

rust-debcargo (2.6.1-3) unstable; urgency=medium

  * Team upload.
  * Bump cargo dependency to 0.70

 -- Fabian Grünbichler <debian@fabian.gruenbichler.email>  Fri, 15 Dec 2023 11:41:50 +0100

rust-debcargo (2.6.1-2) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.6.1 from crates.io using debcargo 2.6.1
  * Bump git2 dependency to 0.18

 -- Peter Michael Green <plugwash@debian.org>  Sun, 03 Dec 2023 16:14:08 +0000

rust-debcargo (2.6.1-1) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.6.1 from crates.io using debcargo 2.6.0

 -- Matthias Geiger <werdahias@riseup.net>  Mon, 20 Nov 2023 15:54:01 +0100

rust-debcargo (2.6.0-3) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.6.0 from crates.io using debcargo 2.6.0
  * Add patch for env-logger 0.10

 -- Matthias Geiger <werdahias@riseup.net>  Thu, 10 Aug 2023 17:43:57 +0200

rust-debcargo (2.6.0-2) unstable; urgency=medium

  * Team upload.
  * Rebuild debcargo 2.6.0 with cargo 0.66.0

 -- Fabian Gruenbichler <debian@fabian.gruenbichler.email>  Thu, 12 Jan 2023 16:33:49 +0000

rust-debcargo (2.6.0-1) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.6.0 from crates.io using debcargo 2.5.0

 -- Fabian Gruenbichler <debian@fabian.gruenbichler.email>  Wed, 16 Nov 2022 10:08:41 +0100

rust-debcargo (2.5.0-5) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.5.0 from crates.io using debcargo 2.5.0
  * Bump textwrap dependency.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 30 Oct 2022 16:37:02 +0000

rust-debcargo (2.5.0-4) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.5.0 from crates.io using debcargo 2.5.0
  * Bump git2 dependency.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 08 Oct 2022 04:27:58 +0000

rust-debcargo (2.5.0-3) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.5.0 from crates.io using debcargo 2.5.0
  * Update dependencies for new textwrap version in Debian.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 14 Mar 2022 01:24:43 +0000

rust-debcargo (2.5.0-2) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.5.0 from crates.io using debcargo 2.5.0
  * Don't run the build-time tests on mips64el as they fail to link.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 29 Nov 2021 11:57:39 +0000

rust-debcargo (2.5.0-1) unstable; urgency=medium

  * Package debcargo 2.5.0 from crates.io using debcargo 2.5.0

 -- Ximin Luo <infinity0@debian.org>  Thu, 11 Nov 2021 13:35:58 +0000

rust-debcargo (2.4.4-1) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.4.4 from crates.io using debcargo 2.4.3
  * Adjust dependencies for versions currently in sid.
  * Revert cargo 0.49 related changes until rust-cargo is
    updated in Debian.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 06 Feb 2021 18:12:33 +0000

rust-debcargo (2.4.3-3) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.4.3 from crates.io using debcargo 2.4.3
  * Bump dependency on git2 package, the old version is broken
    with new libgit2 (Closes: 976658).

 -- Peter Michael Green <plugwash@debian.org>  Mon, 07 Dec 2020 05:21:57 +0000

rust-debcargo (2.4.3-2) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.4.3 from crates.io using debcargo 2.4.2
  * Relax dependency on itertools

 -- Peter Michael Green <plugwash@debian.org>  Tue, 01 Sep 2020 13:47:42 +0000

rust-debcargo (2.4.3-1) unstable; urgency=medium

  * Package debcargo 2.4.3 from crates.io using debcargo 2.4.2

 -- Ximin Luo <infinity0@debian.org>  Sat, 18 Apr 2020 20:52:06 +0100

rust-debcargo (2.4.2-1) unstable; urgency=medium

  * Package debcargo 2.4.2 from crates.io using debcargo 2.4.2

 -- Ximin Luo <infinity0@debian.org>  Wed, 08 Jan 2020 21:54:13 +0000

rust-debcargo (2.4.1-1) unstable; urgency=medium

  * Package debcargo 2.4.1 from crates.io using debcargo 2.4.1
  * Closes: #945436, #945499, #945560, #947088.

 -- Ximin Luo <infinity0@debian.org>  Fri, 03 Jan 2020 01:19:34 +0000

rust-debcargo (2.4.0-1) unstable; urgency=medium

  * Package debcargo 2.4.0 from crates.io using debcargo 2.4.0
    (Closes: #931897)

 -- Ximin Luo <infinity0@debian.org>  Thu, 15 Aug 2019 19:07:57 -0700

rust-debcargo (2.2.10-1) unstable; urgency=medium

  * Package debcargo 2.2.10 from crates.io using debcargo 2.2.10

 -- Ximin Luo <infinity0@debian.org>  Sun, 20 Jan 2019 23:25:15 -0800

rust-debcargo (2.2.9-2) unstable; urgency=medium

  * Package debcargo 2.2.9 from crates.io using debcargo 2.2.9
  * Add dependency on quilt.
  * Install debcargo.toml.example as an example.

 -- Ximin Luo <infinity0@debian.org>  Thu, 29 Nov 2018 22:33:36 -0800

rust-debcargo (2.2.9-1) unstable; urgency=medium

  * Package debcargo 2.2.9 from crates.io using debcargo 2.2.9

 -- Ximin Luo <infinity0@debian.org>  Wed, 14 Nov 2018 21:03:02 -0800
