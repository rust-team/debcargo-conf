rust-gdk4-x11 (0.9.6-1) unstable; urgency=medium

  * Package gdk4-x11 0.9.6 from crates.io using debcargo 2.7.7
  * Refresh patches

 -- Matthias Geiger <werdahias@debian.org>  Tue, 25 Feb 2025 21:55:47 +0100

rust-gdk4-x11 (0.9.5-1) unstable; urgency=medium

  * Package gdk4-x11 0.9.5 from crates.io using debcargo 2.7.5

 -- Matthias Geiger <werdahias@debian.org>  Wed, 25 Dec 2024 21:28:06 +0100

rust-gdk4-x11 (0.9.0-3) unstable; urgency=medium

  * Package gdk4-x11 0.9.0 from crates.io using debcargo 2.7.0
  * Build against gtk4 4.16
  * Build against gtk4 4.16

 -- Matthias Geiger <werdahias@debian.org>  Sat, 21 Sep 2024 23:59:11 +0200

rust-gdk4-x11 (0.9.0-2) unstable; urgency=medium

  * Team upload.
  * Package gdk4-x11 0.9.0 from crates.io using debcargo 2.6.1
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 26 Aug 2024 19:20:21 -0400

rust-gdk4-x11 (0.9.0-1) experimental; urgency=medium

  * Package gdk4-x11 0.9.0 from crates.io using debcargo 2.6.1
  * Depend on gir-rust-code-generator 0.20

 -- Matthias Geiger <werdahias@debian.org>  Sat, 17 Aug 2024 12:48:34 +0200

rust-gdk4-x11 (0.8.2-3) unstable; urgency=medium

  * Explicitly depend on gtk4 4.14 

 -- Matthias Geiger <werdahias@debian.org>  Thu, 01 Aug 2024 17:06:44 +0200

ust-gdk4-x11 (0.8.2-2) unstable; urgency=medium

  * Mark v4_16 feature as broken in debcargo.toml
  * Depend on libgtk-4-dev 

 -- Matthias Geiger <werdahias@riseup.net>  Wed, 15 May 2024 17:37:04 +0200

rust-gdk4-x11 (0.8.2-1) unstable; urgency=medium

  * Package gdk4-x11 0.8.2 from crates.io using debcargo 2.6.1
  * Version dependency on gir-rust-code-generator

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 05 May 2024 11:34:23 +0200

rust-gdk4-x11 (0.8.0-1) experimental; urgency=medium

  * Package gdk4-x11 0.8.0 from crates.io using debcargo 2.6.1
  * Rebased patch for new upstream
  * Properly depend on packages needed for code regeneration
  * Updated copyright years

 -- Matthias Geiger <werdahias@riseup.net>  Tue, 20 Feb 2024 17:53:35 +0100

rust-gdk4-x11 (0.7.2-3) unstable; urgency=medium

  * Team upload
  * Package gdk4-x11 0.7.2 from crates.io using debcargo 2.6.0
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 28 Sep 2023 15:38:17 -0400

rust-gdk4-x11 (0.7.2-2) experimental; urgency=medium

  * Specify dependency on rustc 1.70

 -- Matthias Geiger <werdahias@riseup.net>  Sat, 16 Sep 2023 13:07:22 +0200

rust-gdk4-x11 (0.7.2-1) experimental; urgency=medium

  * Package gdk4-x11 0.7.2 from crates.io using debcargo 2.6.0
  * Regenerate source code with debian tools before build
  * Rebased patches

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 15 Sep 2023 23:02:49 +0200

rust-gdk4-x11 (0.6.3-1) unstable; urgency=medium

  * Package gdk4-x11 0.6.3 from crates.io using debcargo 2.6.0
  * Removed inactive uploader, added my new mail address
  * Added relax-gir-dep.diff patch to enable tests

 -- Matthias Geiger <werdahias@riseup.net>  Wed, 02 Aug 2023 00:27:40 +0200

rust-gdk4-x11 (0.5.4-4) unstable; urgency=medium

  * Package gdk4-x11 0.5.4 from crates.io using debcargo 2.6.0

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sun, 25 Jun 2023 21:03:07 +0200

rust-gdk4-x11 (0.5.4-3) experimental; urgency=medium

  * Package gdk4-x11 0.5.4 from crates.io using debcargo 2.6.0
  * Marked tests as broken in debcargo.toml

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Mon, 29 May 2023 13:38:15 +0200

rust-gdk4-x11 (0.5.4-2) experimental; urgency=medium

  * Package gdk4-x11 0.5.4 from crates.io using debcargo 2.6.0
  * Relax dependency on x11 and gir-format-checker

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sun, 28 May 2023 21:17:37 +0200

rust-gdk4-x11 (0.5.4-1) experimental; urgency=medium

  * Package gdk4-x11 0.5.4 from crates.io using debcargo 2.6.0
  * Added myself to uploaders

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sat, 20 May 2023 15:28:30 +0200

rust-gdk4-x11 (0.3.1-2) unstable; urgency=medium

  * Team upload.
  * Source upload
  * Package gdk4-x11 0.3.1 from crates.io using debcargo 2.5.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 21 May 2022 20:20:21 +0200

rust-gdk4-x11 (0.3.1-1) unstable; urgency=medium

  * Source upload
  * Package gdk4-x11 0.3.1 from crates.io using debcargo 2.4.4

 -- Henry-Nicolas Tourneur <debian@nilux.be>  Sun, 06 Feb 2022 19:08:41 +0100
