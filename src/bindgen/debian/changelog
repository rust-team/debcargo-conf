rust-bindgen (0.71.1-4) unstable; urgency=medium

  * Package bindgen 0.71.1 from crates.io using debcargo 2.7.6
  * Upload to unstable

 -- NoisyCoil <noisycoil@tutanota.com>  Fri, 14 Feb 2025 09:33:38 +0100

rust-bindgen (0.71.1-3) experimental; urgency=medium

  * Package bindgen 0.71.1 from crates.io using debcargo 2.7.6
  * Backport fixes for unsafe_op_in_unsafe_fn

 -- NoisyCoil <noisycoil@tutanota.com>  Tue, 11 Feb 2025 17:51:45 +0100

rust-bindgen (0.71.1-2) unstable; urgency=medium

  * Team upload.
  * Package bindgen 0.71.1 from crates.io using debcargo 2.7.6
  * Add myself to Uploaders

 -- NoisyCoil <noisycoil@tutanota.com>  Thu, 30 Jan 2025 15:39:28 +0100

rust-bindgen (0.71.1-1) experimental; urgency=medium

  * Team upload.
  * Package bindgen 0.71.1 from crates.io using debcargo 2.7.6
  * Update d/copyright
  * Downgrade rust-hash dependency to v1
  * d/patches:
    - drop drop-annotate-snippets.patch (annotate-snippets was updated to
      a more recent version, no need to drop it anymore)
    - refresh

 -- NoisyCoil <noisycoil@tutanota.com>  Sat, 25 Jan 2025 19:24:05 +0000

rust-bindgen (0.70.1-2) unstable; urgency=medium

  * Team upload.
  * Package bindgen 0.70.1 from crates.io using debcargo 2.7.0
  * Adjust criteria for passing time64 related flags to libclang to avoid
    issue with fts api on mips64el.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 21 Nov 2024 19:05:35 +0000

rust-bindgen (0.70.1-1) experimental; urgency=medium

  * Team upload.
  * Package bindgen 0.70.1 from crates.io using debcargo 2.7.2
  * d/patches: drop backported patches, refresh others
  * Unmark passing tests as FLAKY

 -- NoisyCoil <noisycoil@tutanota.com>  Mon, 04 Nov 2024 17:03:16 +0100

rust-bindgen (0.66.1-11) unstable; urgency=medium

  * Team upload.
  * Package bindgen 0.66.1 from crates.io using debcargo 2.7.2
  * d/patches: add use-clang-getfilelocation.patch - fix SourceLocation::location
    for libclang >= 19.1.0 (Closes: #1086510)

 -- NoisyCoil <noisycoil@tutanota.com>  Fri, 01 Nov 2024 20:04:35 +0100

rust-bindgen (0.66.1-10) unstable; urgency=medium

  * Package bindgen 0.66.1 from crates.io using debcargo 2.7.0
  * Relax dependency on which.

  [ NoisyCoil ]
  * Team upload.
  * Package bindgen 0.66.1 from crates.io using debcargo 2.7.0
  * d/debcargo.toml: library: do not depend on clang, do not suggest
    source package

 -- Peter Michael Green <plugwash@debian.org>  Sun, 13 Oct 2024 14:26:40 +0000

rust-bindgen (0.66.1-9) unstable; urgency=medium

  * Team upload.
  * Package bindgen 0.66.1 from crates.io using debcargo 2.7.0
  * Add upstream patch avoiding align when packed is needed (Closes:
    #1078698)

 -- Matthias Geiger <werdahias@debian.org>  Wed, 02 Oct 2024 23:57:14 +0200

rust-bindgen (0.66.1-8) unstable; urgency=medium

  * Team upload.
  * Package bindgen 0.66.1 from crates.io using debcargo 2.7.0

  [ Fabian Grünbichler ]
  * Drop annotate-snippets dependency of experimental feature

 -- Peter Michael Green <plugwash@debian.org>  Wed, 02 Oct 2024 00:15:18 +0000

rust-bindgen (0.66.1-7) unstable; urgency=medium

  * Team upload.
  * Dropped hard dependecies on clang-15. This was done as LLVM
    was not building on mips64el in the past; since it builds now there
    this is obsolete

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 21 Jun 2024 22:42:34 +0200

rust-bindgen (0.66.1-6) unstable; urgency=medium

  * d/patches: backport "codegen: Generate CStr only if possible" from v0.68.0
    (Closes: #1069047)

 -- NoisyCoil <noisycoil@tutanota.com>  Sat, 08 Jun 2024 23:56:07 +0200

rust-bindgen (0.66.1-5) unstable; urgency=medium

  * Team upload.
  * Package bindgen 0.66.1 from crates.io using debcargo 2.6.1
  * Pass time64 defines to clang (on architectures other than i386).

 -- Peter Michael Green <plugwash@debian.org>  Tue, 07 May 2024 01:14:46 +0000

rust-bindgen (0.66.1-4) unstable; urgency=medium

  * Team upload.
  * Package bindgen 0.66.1 from crates.io using debcargo 2.6.1
  * Workaround mips64el llvm breakage in trixie.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 08 Jan 2024 01:13:01 +0000

rust-bindgen (0.66.1-3) unstable; urgency=medium

  * Team upload.
  * Package bindgen 0.66.1 from crates.io using debcargo 2.6.0
  * Really upload to unstable.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 19 Sep 2023 20:34:05 +0000

rust-bindgen (0.66.1-2) experimental; urgency=medium

  * Team upload.
  * Package bindgen 0.66.1 from crates.io using debcargo 2.6.0
  * Upload to unstable.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 19 Sep 2023 20:26:11 +0000

rust-bindgen (0.66.1-1) experimental; urgency=medium

  * Package bindgen 0.66.1 from crates.io using debcargo 2.6.0
  * Drop relax-env-logger.diff

  [ Matthias Geiger ]
  * Team upload.
  * Package bindgen 0.66.1 from crates.io using debcargo 2.6.0

 -- Peter Michael Green <plugwash@debian.org>  Sat, 02 Sep 2023 15:48:30 +0000

rust-bindgen (0.60.1-3) unstable; urgency=medium

  * Team upload.
  * Package bindgen 0.60.1 from crates.io using debcargo 2.6.0
  * Add patch for env-logger 0.10

 -- Matthias Geiger <werdahias@riseup.net>  Thu, 10 Aug 2023 17:40:02 +0200

rust-bindgen (0.60.1-2) unstable; urgency=medium

  * Team upload.
  * Package bindgen 0.60.1 from crates.io using debcargo 2.5.0
  * Drop lower-dep.diff - shlex has been updated.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 31 Jul 2022 17:59:04 +0000

rust-bindgen (0.60.1-1) unstable; urgency=medium

  * Team upload.
  * Package bindgen 0.60.1 from crates.io using debcargo 2.5.0
  * Upgrade to a more recent version of clang (Closes: #1000911)

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 06 Jun 2022 16:19:28 +0200

rust-bindgen (0.59.2-2) unstable; urgency=medium

  * Team upload.
  * Package bindgen 0.59.2 from crates.io using debcargo 2.5.0
  * Upload to unstable

 -- James McCoy <jamessan@debian.org>  Sat, 05 Mar 2022 16:09:10 -0500

rust-bindgen (0.59.2-1) experimental; urgency=medium

  * Team upload.
  * Package bindgen 0.59.2 from crates.io using debcargo 2.5.0

 -- James McCoy <jamessan@debian.org>  Tue, 22 Feb 2022 20:59:44 -0500

rust-bindgen (0.59.1-2) unstable; urgency=medium

  * Team upload.
  * Package bindgen 0.59.1 from crates.io using debcargo 2.4.4
    + Regenerating packaging with debcargo will allow rust-bindgen+env-logger
      to be installed with both old and new versions of rust-env-logger.

 -- Peter Michael Green <plugwash@debian.org>  Fri, 12 Nov 2021 02:44:31 +0000

rust-bindgen (0.59.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * add patch to relax dependency on Rust library env_logger,
    and update (build-)dependencies on librust-env-logger-*-dev;
    this closes: bug#998347
  * fix Cargo.toml checksum hint file

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 02 Nov 2021 19:36:53 +0100

rust-bindgen (0.59.1-1) unstable; urgency=medium

  * Team upload.
  * Package bindgen 0.59.1 from crates.io using debcargo 2.4.4-alpha.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 22 Aug 2021 17:37:34 +0200

rust-bindgen (0.55.1-3) unstable; urgency=medium

  * Team upload.
  * Package bindgen 0.55.1 from crates.io using debcargo 2.4.3
  * Mark tests for "runtime" and "static" featuresets as broken
    (Downgrads: 973387)

 -- Peter Michael Green <plugwash@debian.org>  Sat, 31 Oct 2020 17:42:27 +0000

rust-bindgen (0.55.1-2) unstable; urgency=medium

  * Team upload.
  * Source upload
  * Package bindgen 0.55.1 from crates.io using debcargo 2.4.3

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 18 Oct 2020 11:09:55 +0200

rust-bindgen (0.55.1-1) unstable; urgency=medium

  * Team upload.
  * Source upload
  * Package bindgen 0.55.1 from crates.io using debcargo 2.4.2
  * No longer needs the env-logger bump

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 29 Aug 2020 18:48:29 +0200

rust-bindgen (0.51.1-5) unstable; urgency=medium

  * Team upload.
  * Package bindgen 0.51.1 from crates.io using debcargo 2.4.2
  * Fix the dependencies (don't want for the NEW approval)
    (Closes: #971139)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 04 Oct 2020 17:08:45 +0200

rust-bindgen (0.51.1-4) unstable; urgency=medium

  * Team upload.
  * Source upload
  * Package bindgen 0.51.1 from crates.io using debcargo 2.4.0
  * Bump env-logger dep

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 31 Dec 2019 18:27:17 +0100

rust-bindgen (0.51.1-3) unstable; urgency=medium

  * mark feature-specific autopkgtests as flakey (see #945560)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 13 Dec 2019 14:14:07 -0500

rust-bindgen (0.51.1-2) unstable; urgency=medium

  * source-only release, no changes

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 23 Oct 2019 15:52:09 -0400

rust-bindgen (0.51.1-1) unstable; urgency=medium

  * Source upload
  * Package bindgen 0.51.1 from crates.io using debcargo 2.4.0

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 22 Oct 2019 05:59:46 -0400

rust-bindgen (0.47.0-1) unstable; urgency=medium

  * Source upload
  * Package bindgen 0.47.0 from crates.io using debcargo 2.2.9

 -- Ximin Luo <infinity0@debian.org>  Sat, 19 Jan 2019 12:54:50 -0800

rust-bindgen (0.45.0-1) unstable; urgency=medium

  * Source upload
  * Package bindgen 0.45.0 from crates.io using debcargo 2.2.9

 -- Ximin Luo <infinity0@debian.org>  Wed, 26 Dec 2018 11:03:47 -0800
