rust-gobject-sys (0.20.9-1) unstable; urgency=medium

  * Team upload
  * Package gobject-sys 0.20.9 from crates.io using debcargo 2.7.6

 -- Jeremy Bícha <jbicha@ubuntu.com>  Sun, 16 Feb 2025 14:34:49 -0500

rust-gobject-sys (0.20.7-1) unstable; urgency=medium

  * Package gobject-sys 0.20.7 from crates.io using debcargo 2.7.5

 -- Matthias Geiger <werdahias@debian.org>  Thu, 26 Dec 2024 13:42:44 +0100

rust-gobject-sys (0.20.4-1) unstable; urgency=medium

  * Package gobject-sys 0.20.4 from crates.io using debcargo 2.7.0

 -- Matthias Geiger <werdahias@debian.org>  Mon, 30 Sep 2024 16:48:13 +0200

rust-gobject-sys (0.20.1-1) unstable; urgency=medium

  * Team upload
  * Package gobject-sys 0.20.1 from crates.io using debcargo 2.6.1

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 2 Sep 2024 09:40:31 -0400

rust-gobject-sys (0.20.0-2) unstable; urgency=medium

  * Team upload.
  * Package gobject-sys 0.20.0 from crates.io using debcargo 2.6.1
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 26 Aug 2024 13:24:55 -0400

rust-gobject-sys (0.20.0-1) experimental; urgency=medium

  * Depend on gir-rust-generator >= 0.20 
  * Package gobject-sys 0.20.0 from crates.io using debcargo 2.6.1

 -- Matthias Geiger <werdahias@debian.org>  Sat, 10 Aug 2024 15:03:06 +0200

rust-gobject-sys (0.19.5-3) unstable; urgency=medium

  * Fix depends in debcargo.toml 

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 05 May 2024 16:27:40 +0200

rust-gobject-sys (0.19.5-2) unstable; urgency=medium

  * Upload to unstable

 -- Matthias Geiger <werdahias@riseup.net>  Sat, 04 May 2024 17:43:13 +0200

rust-gobject-sys (0.19.5-1) experimental; urgency=medium

  * Package gobject-sys 0.19.5 from crates.io using debcargo 2.6.1
  * Bump gir-rust-code-generator dependency to 0.19.1
  * Version test-dependency on libglib-2.0-dev
  * Stopped marking v2.80 feature as broken

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 03 May 2024 17:19:50 +0200

rust-gobject-sys (0.19.0-2) experimental; urgency=medium

  * Mark v2_80 feature as broken 

 -- Matthias Geiger <werdahias@riseup.net>  Thu, 08 Feb 2024 18:57:57 +0100

rust-gobject-sys (0.19.0-1) experimental; urgency=medium

  * Package gobject-sys 0.19.0 from crates.io using debcargo 2.6.1
  * Updated copyright years and source url
  * Properly build-depend on packages needed for regenerization
  * Version dependency on gir-rust-code-generator
  * Test-depend on libglib2.0-dev

 -- Matthias Geiger <werdahias@riseup.net>  Thu, 08 Feb 2024 12:38:28 +0100

rust-gobject-sys (0.18.0-2) unstable; urgency=medium

  * Team upload
  * Drop obsolete MSRV patch
  * Package gobject-sys 0.18.0 from crates.io using debcargo 2.6.0
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 28 Sep 2023 14:43:39 -0400

rust-gobject-sys (0.18.0-1) experimental; urgency=medium

  * Package gobject-sys 0.18.0 from crates.io using debcargo 2.6.0
  * Included patch for MSRV downgrade
  * Regenerate source code with debian tools before build

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 08 Sep 2023 20:29:48 +0200

rust-gobject-sys (0.17.10-1) unstable; urgency=medium

  * Team upload.
  * Package gobject-sys 0.17.10 from crates.io using debcargo 2.6.0
  * Removed inactive uploader, added my new mail address
  * Include patch to pass autopkgtest, removed annotation that tests are
    broken

 -- Matthias Geiger <werdahias@riseup.net>  Tue, 01 Aug 2023 23:51:36 +0200

rust-gobject-sys (0.16.3-2) unstable; urgency=medium

  * Package gobject-sys 0.16.3 from crates.io using debcargo 2.6.0

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sun, 25 Jun 2023 20:34:33 +0200

rust-gobject-sys (0.16.3-1) experimental; urgency=medium

  * Package gobject-sys 0.16.3 from crates.io using debcargo 2.6.0
  * Added myself to uploaders
  * Added collapse_features
  * Dropped obsolete patches

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sat, 20 May 2023 15:21:39 +0200

rust-gobject-sys (0.14.0-2) unstable; urgency=medium

  * Team upload.
  * debian/patches: Update to system-deps 6

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 05 Oct 2022 09:39:10 +0200

rust-gobject-sys (0.14.0-1) unstable; urgency=medium

  * Team upload.
  * Package gobject-sys 0.14.0 from crates.io using debcargo 2.4.4

 -- Henry-Nicolas Tourneur <debian@nilux.be>  Sun, 05 Dec 2021 22:45:46 +0100

rust-gobject-sys (0.9.1-1) experimental; urgency=medium

  * Package gobject-sys 0.9.1 from crates.io using debcargo 2.4.0

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Tue, 17 Dec 2019 11:03:13 +0100

rust-gobject-sys (0.9.0-2) unstable; urgency=medium

  * Team upload.
  * Package gobject-sys 0.9.0 from crates.io using debcargo 2.2.10
  * Source upload for migration

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 06 Aug 2019 18:23:30 +0200

rust-gobject-sys (0.9.0-1) unstable; urgency=medium

  * Package gobject-sys 0.9.0 from crates.io using debcargo 2.3.0

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Mon, 08 Jul 2019 05:20:56 +0200

rust-gobject-sys (0.7.0-1) unstable; urgency=medium

  * Package gobject-sys 0.7.0 from crates.io using debcargo 2.2.8

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Sun, 04 Nov 2018 09:50:50 +0100
