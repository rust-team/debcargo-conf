Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: blake3
Upstream-Contact: Jack O'Connor <oconnor663@gmail.com>
 Samuel Neves <sneves@dei.uc.pt>
 Jean-Philippe Aumasson> <jeanphilippe.aumasson@gmail.com>
 Zooko <zooko@z.cash>
Source: https://github.com/BLAKE3-team/BLAKE3

Files: *
Copyright: 2020 Jack O'Connor <oconnor663@gmail.com>
 2020 Samuel Neves
License: CC0-1.0 or Apache-2.0 or Apache-2.0-with-LLVM-exception

Files: debian/*
Copyright: 2020 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
License: CC0-1.0 or Apache-2.0 or Apache-2.0-with-LLVM-exception

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: Apache-2.0-with-LLVM-exception
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0.
 In addition, this license has the following exception:
 .
 ---- LLVM Exceptions to the Apache 2.0 License ----
 .
 As an exception, if, as a result of your compiling your source code, portions
 of this Software are embedded into an Object form of such source code, you
 may redistribute such embedded portions in such Object form without complying
 with the conditions of Sections 4(a), 4(b) and 4(d) of the License.
 .
 In addition, if you combine or link compiled forms of this Software with
 software that is licensed under the GPLv2 ("Combined Software") and if a
 court of competent jurisdiction determines that the patent provision (Section
 3), the indemnity provision (Section 9) or other Section of the License
 conflicts with the conditions of the GPLv2, you may retroactively and
 prospectively choose to deem waived or otherwise exclude such Section(s) of
 the License, but only in their entirety and only with respect to the Combined
 Software.

License: CC0-1.0
 Debian systems provide the CC0 1.0 Universal License in
 /usr/share/common-licenses/CC0-1.0
