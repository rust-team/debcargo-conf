rust-glycin-utils (3.0.0~rc-1) unstable; urgency=medium

  * Package glycin-utils 3.0.0-rc from crates.io using debcargo 2.7.7

 -- Matthias Geiger <werdahias@debian.org>  Tue, 04 Mar 2025 01:01:35 +0100

rust-glycin-utils (3.0.0~beta-2) unstable; urgency=medium

  * Upload to unstable (Closes: #1094111) 

 -- Matthias Geiger <werdahias@debian.org>  Thu, 13 Feb 2025 16:32:52 +0100

rust-glycin-utils (3.0.0~beta-1) experimental; urgency=medium

  * Package glycin-utils 3.0.0-beta from crates.io using debcargo 2.7.6

 -- Matthias Geiger <werdahias@debian.org>  Mon, 03 Feb 2025 17:16:33 +0100

rust-glycin-utils (3.0.0~alpha.1-1) unstable; urgency=medium

  * Package glycin-utils 3.0.0-alpha.1 from crates.io using debcargo 2.7.6
  * Stop patching nix

 -- Matthias Geiger <werdahias@debian.org>  Tue, 21 Jan 2025 23:04:44 +0100

rust-glycin-utils (2.0.2-2) unstable; urgency=medium

  * Team upload.
  * Package glycin-utils 2.0.2 from crates.io using debcargo 2.7.5
  * Bump env_logger dependency to v0.11

 -- NoisyCoil <noisycoil@tutanota.com>  Tue, 31 Dec 2024 13:19:08 +0100

rust-glycin-utils (2.0.2-1) unstable; urgency=medium

  * Package glycin-utils 2.0.2 from crates.io using debcargo 2.7.5

 -- Matthias Geiger <werdahias@debian.org>  Thu, 05 Dec 2024 10:51:58 +0100

rust-glycin-utils (2.0.0-4) unstable; urgency=medium

  * Stop patching image

 -- Matthias Geiger <werdahias@debian.org>  Sat, 09 Nov 2024 17:56:15 +0100

rust-glycin-utils (2.0.0-3) unstable; urgency=medium

  * Team upload.
  * Package glycin-utils 2.0.0 from crates.io using debcargo 2.7.0
  * Relax nix dependency.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 22 Oct 2024 13:42:21 +0000

rust-glycin-utils (2.0.0-2) unstable; urgency=medium

  * Upload to unstable

 -- Matthias Geiger <werdahias@debian.org>  Fri, 20 Sep 2024 12:16:31 +0200

rust-glycin-utils (2.0.0-1) experimental; urgency=medium

  * Package glycin-utils 2.0.0 from crates.io using debcargo 2.6.1
  * Refresh patches; drop autoexamples patch (included in upstream release) 

 -- Matthias Geiger <werdahias@debian.org>  Tue, 17 Sep 2024 14:23:06 +0200

rust-glycin-utils (2.0.0~beta-1) experimental; urgency=medium

  * Package glycin-utils 2.0.0-beta from crates.io using debcargo 2.6.1
  * Refresh patch 
  * Mark autopkgtest as flaky for now since this is a beta release

 -- Matthias Geiger <werdahias@debian.org>  Fri, 23 Aug 2024 17:35:11 +0200

rust-glycin-utils (1.0.1-3) unstable; urgency=medium

  * Team upload.
  * Package glycin-utils 1.0.1 from crates.io using debcargo 2.6.1
  * Relax dependency on futures-lite.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 13 Aug 2024 18:51:56 +0000

rust-glycin-utils (1.0.1-2) unstable; urgency=medium

  * Upload to unstable 

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 14 Jul 2024 15:14:29 +0200

rust-glycin-utils (1.0.1-1) experimental; urgency=medium

  * Package glycin-utils 1.0.1 from crates.io using debcargo 2.6.1
  * Drop nix-0.27 patch, included in new upstream release
  * Refresh relax-deps patch

 -- Matthias Geiger <werdahias@riseup.net>  Thu, 04 Jul 2024 23:08:29 +0200

rust-glycin-utils (0.1.0-3) unstable; urgency=medium

  * Team upload.
  * Package glycin-utils 0.1.0 from crates.io using debcargo 2.6.1

 -- Peter Michael Green <plugwash@debian.org>  Thu, 02 May 2024 15:09:14 +0000

rust-glycin-utils (1.0.0~alpha.5-1) experimental; urgency=medium

   * Package glycin-utils 1.0.0-alpha.5 from crates.io using debcargo 2.6.1
   * Rebased patch for new upstream
   * Temporarly disabled tests because debcargo can't deal with beta
     versions yet
   * Upload to experimental

 -- Matthias Geiger <werdahias@riseup.net> Mon, 29 Jan 2024 23:55:45 +0100

rust-glycin-utils (0.1.0-2) unstable; urgency=medium

  * Team upload.
  * Package glycin-utils 0.1.0 from crates.io using debcargo 2.6.1
  * Bump memmap2 dependency to 0.9

 -- Peter Michael Green <plugwash@debian.org>  Sat, 02 Dec 2023 14:07:09 +0000

rust-glycin-utils (0.1.0-1) unstable; urgency=medium

  * Package glycin-utils 0.1.0 from crates.io using debcargo 2.6.0
  * Enabled tests.
  * Rebased patches.

 -- Matthias Geiger <werdahias@riseup.net>  Sat, 16 Sep 2023 19:30:50 +0200

rust-glycin-utils (0.1.0~rc-1) unstable; urgency=medium

  * Package glycin-utils 0.1.0-rc from crates.io using debcargo 2.6.0
  * Updated relax-deps patch

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 08 Sep 2023 22:53:05 +0200

rust-glycin-utils (0.1.0~beta.4-1) experimental; urgency=medium

  * Package glycin-utils 0.1.0-beta.4 from crates.io using debcargo 2.6.0

 -- Matthias Geiger <werdahias@riseup.net>  Thu, 31 Aug 2023 17:54:22 -0400
