rust-async-compression (0.4.13-1) unstable; urgency=medium

  * Package async-compression 0.4.13 from crates.io using debcargo 2.7.0
  * Stop excluding examples and tests
  * Add patch to relax brotli dependency
  * Drop patch removing examples; autoexamples is set to false

 -- Matthias Geiger <werdahias@debian.org>  Sat, 05 Oct 2024 01:27:32 +0200

rust-async-compression (0.4.12-1) unstable; urgency=medium

  * Refresh patches so some tests can run 

  [ Blair Noctis ]
  * Team upload.
  * Package async-compression 0.4.12 from crates.io using debcargo 2.6.1
  * Drop disable-deflate64.patch, dependency now packaged
  * Relax dep on proptest-derive

 -- Matthias Geiger <werdahias@debian.org>  Mon, 30 Sep 2024 16:25:13 +0200

rust-async-compression (0.4.11-3) unstable; urgency=medium

  * Team upload.
  * Drop relax-brotli.diff to sync with brotli 6.0

 -- Blair Noctis <n@sail.ng>  Mon, 17 Jun 2024 21:14:33 +0200

rust-async-compression (0.4.11-2) unstable; urgency=medium

  [ Holger Levsen ]
  * Team upload.

  [ Matthias Geiger ]
  * Package async-compression 0.4.11 from crates.io using debcargo 2.6.1
  * Patch brotli dependency (Closes: #1073284)
  * Updated my mail address in debcargo.toml
  * Patch ntest dependency so tests can run
  * Skip currently broken features

 -- Holger Levsen <holger@debian.org>  Mon, 17 Jun 2024 10:23:18 +0200

rust-async-compression (0.4.11-1) unstable; urgency=medium

  * Package async-compression 0.4.11 from crates.io using debcargo 2.6.1

  [ Blair Noctis ]
  * Package async-compression 0.4.7 from crates.io using debcargo 2.6.1

 -- Holger Levsen <holger@debian.org>  Sun, 16 Jun 2024 13:58:04 +0200

rust-async-compression (0.4.5-1) experimental; urgency=medium

  * Team upload.
  * Package async-compression 0.4.5 from crates.io using debcargo 2.6.1
  * Disable deflate64, unpackaged
  * Remove zstdmt feature, removed in zstd-safe crate

 -- Blair Noctis <n@sail.ng>  Fri, 01 Dec 2023 02:18:32 +0000

rust-async-compression (0.4.0-2) unstable; urgency=medium

  * Enable brotli feature.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 23 Oct 2023 13:10:21 +0100

rust-async-compression (0.4.0-1) unstable; urgency=medium

  * Package async-compression 0.4.0 from crates.io using debcargo 2.6.0

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Tue, 12 Sep 2023 21:44:02 +0200
