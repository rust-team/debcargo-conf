rust-glycin (2.1.0~rc-1) unstable; urgency=medium

  * Package glycin 2.1.0-rc from crates.io using debcargo 2.7.7

 -- Matthias Geiger <werdahias@debian.org>  Fri, 28 Feb 2025 21:08:06 +0100

rust-glycin (2.1.0~beta-4) unstable; urgency=medium

  * Drop patch fixing build; this was needed only as work-around and is
    obsolete

 -- Matthias Geiger <werdahias@debian.org>  Sun, 16 Feb 2025 18:10:36 +0100

rust-glycin (2.1.0~beta-3) unstable; urgency=medium

  * Upload to unstable 

 -- Matthias Geiger <werdahias@debian.org>  Thu, 13 Feb 2025 16:34:51 +0100

rust-glycin (2.1.0~beta-2) experimental; urgency=medium

  * Package glycin 2.1.0-beta from crates.io using debcargo 2.7.6

 -- Matthias Geiger <werdahias@debian.org>  Thu, 06 Feb 2025 19:47:42 +0100

rust-glycin (2.1.0~beta-1) experimental; urgency=medium

  * Package glycin 2.1.0-beta from crates.io using debcargo 2.7.6

 -- Matthias Geiger <werdahias@debian.org>  Mon, 03 Feb 2025 17:17:47 +0100

rust-glycin (2.1.0~alpha.1-1) experimental; urgency=medium

  * Package glycin 2.1.0-alpha.1 from crates.io using debcargo 2.7.6

 -- Matthias Geiger <werdahias@debian.org>  Tue, 21 Jan 2025 20:37:21 +0100

rust-glycin (2.0.3-1) unstable; urgency=medium

  * Package glycin 2.0.3 from crates.io using debcargo 2.7.6

 -- Matthias Geiger <werdahias@debian.org>  Thu, 09 Jan 2025 10:22:21 +0100

rust-glycin (2.0.2-2) unstable; urgency=medium

  * Team upload.
  * Package glycin 2.0.2 from crates.io using debcargo 2.7.5
  * Bump yeslogic-fontconfig-sys version from 3.0.0 to 5.0.0

 -- James McCoy <jamessan@debian.org>  Wed, 01 Jan 2025 11:10:24 -0500

rust-glycin (2.0.2-1) unstable; urgency=medium

  * Team upload.
  * Package glycin 2.0.2 from crates.io using debcargo 2.7.0

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 25 Nov 2024 14:20:30 -0500

rust-glycin (2.0.1-3) unstable; urgency=medium

  * Team upload.
  * Package glycin 2.0.1 from crates.io using debcargo 2.7.0
  * Relax nix dependency.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 22 Oct 2024 15:48:18 +0000

rust-glycin (2.0.1-2) unstable; urgency=medium

  * Upload to unstable

 -- Matthias Geiger <werdahias@debian.org>  Fri, 20 Sep 2024 12:17:49 +0200

rust-glycin (2.0.1-1) experimental; urgency=medium

  * Package glycin 2.0.1 from crates.io using debcargo 2.6.1
  * Refresh patches, drop autoexamples patch
  * Mark some features as flaky

 -- Matthias Geiger <werdahias@debian.org>  Tue, 17 Sep 2024 14:56:52 +0200

rust-glycin (2.0.0~beta-2) experimental; urgency=medium

  * relax-deps patch: patch tokio-stream, too

 -- Matthias Geiger <werdahias@debian.org>  Fri, 23 Aug 2024 19:15:06 +0200

rust-glycin (2.0.0~beta-1) experimental; urgency=medium

  * Package glycin 2.0.0-beta from crates.io using debcargo 2.6.1
  * Add patch to relax dependencies
  * Add patches to disable doctests and examples

 -- Matthias Geiger <werdahias@debian.org>  Fri, 23 Aug 2024 18:12:51 +0200

rust-glycin (1.0.1-3) unstable; urgency=medium

  * Team upload
  * Package glycin 1.0.1 from crates.io using debcargo 2.6.1
  * Build with glib 0.20

 -- Jeremy Bícha <jbicha@ubuntu.com>  Tue, 27 Aug 2024 13:20:45 -0400

rust-glycin (1.0.1-2) unstable; urgency=medium

  * Upload to unstable

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 14 Jul 2024 15:12:27 +0200

rust-glycin (1.0.1-1) experimental; urgency=medium

  * Package glycin 1.0.1 from crates.io using debcargo 2.6.1
  * Drop patches, obsoleted
  * Collapsed features in debcargo.toml

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 05 Jul 2024 08:40:03 +0200

rust-glycin (0.1.0-5) unstable; urgency=medium

  * Team upload.
  * Package glycin 0.1.0 from crates.io using debcargo 2.6.1

 -- Peter Michael Green <plugwash@debian.org>  Thu, 02 May 2024 15:15:40 +0000

rust-glycin (0.1.0-4) unstable; urgency=medium

  * Package glycin 0.1.0 from crates.io using debcargo 2.6.1
  * Cherry-pick upstream patch for newer lcms2
  * Rebase relax-deps patch

 -- Matthias Geiger <werdahias@riseup.net>  Wed, 13 Dec 2023 18:18:35 +0100

rust-glycin (0.1.0-3) unstable; urgency=medium

  * Team upload.
  * Package glycin 0.1.0 from crates.io using debcargo 2.6.1
  * Bump memmap2 dependency to 0.9

 -- Peter Michael Green <plugwash@debian.org>  Sat, 02 Dec 2023 14:12:14 +0000

rust-glycin (0.1.0-2) unstable; urgency=medium

  * Team upload
  * Package glycin 0.1.0 from crates.io using debcargo 2.6.0
  * Release to Unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Tue, 24 Oct 2023 08:04:17 -0400

rust-glycin (0.1.0-1) experimental; urgency=medium

  * Package glycin 0.1.0 from crates.io using debcargo 2.6.0

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 29 Sep 2023 09:28:48 -0400
