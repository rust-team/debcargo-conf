Source: rust-ntpd
Section: utils
Priority: optional
Build-Depends: debhelper-compat (= 13),
 dh-sequence-cargo,
 help2man
Build-Depends-Arch: cargo:native,
 rustc:native (>= 1.70),
 libstd-rust-dev,
 librust-async-trait-0.1+default-dev (>= 0.1.67-~~),
 librust-clock-steering-0.2+default-dev (>= 0.2.1-~~),
 librust-libc-0.2+default-dev (>= 0.2.145-~~),
 librust-ntp-proto-1+--internal-api-dev (>= 1.4.0-~~),
 librust-ntp-proto-1+rustls23-dev (>= 1.4.0-~~),
 librust-rand-0.8+default-dev,
 librust-serde-1+default-dev (>= 1.0.145-~~),
 librust-serde-1+derive-dev (>= 1.0.145-~~),
 librust-serde-json-1+default-dev,
 librust-timestamped-socket-0.2+default-dev (>= 0.2.2-~~),
 librust-tokio-1+default-dev (>= 1.32-~~),
 librust-tokio-1+fs-dev (>= 1.32-~~),
 librust-tokio-1+io-std-dev (>= 1.32-~~),
 librust-tokio-1+io-util-dev (>= 1.32-~~),
 librust-tokio-1+macros-dev (>= 1.32-~~),
 librust-tokio-1+net-dev (>= 1.32-~~),
 librust-tokio-1+rt-multi-thread-dev (>= 1.32-~~),
 librust-tokio-1+sync-dev (>= 1.32-~~),
 librust-toml+parse-dev (<< 0.9.0-~~),
 librust-toml+parse-dev (>= 0.6.0-~~),
 librust-tracing-0.1+default-dev (>= 0.1.37-~~),
 librust-tracing-subscriber-0.3+ansi-dev,
 librust-tracing-subscriber-0.3+fmt-dev,
 librust-tracing-subscriber-0.3+std-dev
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Sylvestre Ledru <sylvestre@debian.org>
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/ntpd]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/ntpd
Homepage: https://github.com/pendulum-project/ntpd-rs
X-Cargo-Crate: ntpd
Rules-Requires-Root: no

Package: librust-ntpd-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-async-trait-0.1+default-dev (>= 0.1.67-~~),
 librust-clock-steering-0.2+default-dev (>= 0.2.1-~~),
 librust-libc-0.2+default-dev (>= 0.2.145-~~),
 librust-ntp-proto-1+--internal-api-dev (>= 1.4.0-~~),
 librust-ntp-proto-1+ntpv5-dev (>= 1.4.0-~~),
 librust-ntp-proto-1+nts-pool-dev (>= 1.4.0-~~),
 librust-ntp-proto-1+rustls23-dev (>= 1.4.0-~~),
 librust-rand-0.8+default-dev,
 librust-rustls-0.23+logging-dev,
 librust-rustls-0.23+ring-dev,
 librust-rustls-0.23+std-dev,
 librust-rustls-0.23+tls12-dev,
 librust-serde-1+default-dev (>= 1.0.145-~~),
 librust-serde-1+derive-dev (>= 1.0.145-~~),
 librust-serde-json-1+default-dev,
 librust-timestamped-socket-0.2+default-dev (>= 0.2.2-~~),
 librust-tokio-1+default-dev (>= 1.32-~~),
 librust-tokio-1+fs-dev (>= 1.32-~~),
 librust-tokio-1+io-std-dev (>= 1.32-~~),
 librust-tokio-1+io-util-dev (>= 1.32-~~),
 librust-tokio-1+macros-dev (>= 1.32-~~),
 librust-tokio-1+net-dev (>= 1.32-~~),
 librust-tokio-1+rt-multi-thread-dev (>= 1.32-~~),
 librust-tokio-1+sync-dev (>= 1.32-~~),
 librust-tokio-rustls-0.26+logging-dev,
 librust-tokio-rustls-0.26+ring-dev,
 librust-tokio-rustls-0.26+tls12-dev,
 librust-toml+parse-dev (<< 0.9.0-~~),
 librust-toml+parse-dev (>= 0.6.0-~~),
 librust-tracing-0.1+default-dev (>= 0.1.37-~~),
 librust-tracing-subscriber-0.3+ansi-dev,
 librust-tracing-subscriber-0.3+fmt-dev,
 librust-tracing-subscriber-0.3+std-dev
Provides:
 librust-ntpd+default-dev (= ${binary:Version}),
 librust-ntpd+hardware-timestamping-dev (= ${binary:Version}),
 librust-ntpd+run-tokio-rustls-tests-dev (= ${binary:Version}),
 librust-ntpd+rustls23-dev (= ${binary:Version}),
 librust-ntpd+unstable-ntpv5-dev (= ${binary:Version}),
 librust-ntpd+unstable-nts-pool-dev (= ${binary:Version}),
 librust-ntpd-1-dev (= ${binary:Version}),
 librust-ntpd-1+default-dev (= ${binary:Version}),
 librust-ntpd-1+hardware-timestamping-dev (= ${binary:Version}),
 librust-ntpd-1+run-tokio-rustls-tests-dev (= ${binary:Version}),
 librust-ntpd-1+rustls23-dev (= ${binary:Version}),
 librust-ntpd-1+unstable-ntpv5-dev (= ${binary:Version}),
 librust-ntpd-1+unstable-nts-pool-dev (= ${binary:Version}),
 librust-ntpd-1.4-dev (= ${binary:Version}),
 librust-ntpd-1.4+default-dev (= ${binary:Version}),
 librust-ntpd-1.4+hardware-timestamping-dev (= ${binary:Version}),
 librust-ntpd-1.4+run-tokio-rustls-tests-dev (= ${binary:Version}),
 librust-ntpd-1.4+rustls23-dev (= ${binary:Version}),
 librust-ntpd-1.4+unstable-ntpv5-dev (= ${binary:Version}),
 librust-ntpd-1.4+unstable-nts-pool-dev (= ${binary:Version}),
 librust-ntpd-1.4.0-dev (= ${binary:Version}),
 librust-ntpd-1.4.0+default-dev (= ${binary:Version}),
 librust-ntpd-1.4.0+hardware-timestamping-dev (= ${binary:Version}),
 librust-ntpd-1.4.0+run-tokio-rustls-tests-dev (= ${binary:Version}),
 librust-ntpd-1.4.0+rustls23-dev (= ${binary:Version}),
 librust-ntpd-1.4.0+unstable-ntpv5-dev (= ${binary:Version}),
 librust-ntpd-1.4.0+unstable-nts-pool-dev (= ${binary:Version})
Description: Full-featured implementation of NTP with NTS support - Rust source code
 Source code for Debianized Rust crate "ntpd"

Package: ntpd
Architecture: any
Multi-Arch: allowed
Section: utils
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 ${cargo:Depends},
 adduser
Recommends:
 ${cargo:Recommends}
Suggests:
 ${cargo:Suggests}
Provides:
 ${cargo:Provides}
Built-Using: ${cargo:Built-Using}
Static-Built-Using: ${cargo:Static-Built-Using}
Description: Rust-based NTP implementation with NTS support
 ntpd-rs is an NTP implementation written in Rust,
 emphasizing security and stability.
 It provides both client and server functionalities and
 supports NTS.
