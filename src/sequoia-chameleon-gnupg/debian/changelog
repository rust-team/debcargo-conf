rust-sequoia-chameleon-gnupg (0.12.0-1) unstable; urgency=medium

  * Package sequoia-chameleon-gnupg 0.12.0 from crates.io using debcargo 2.7.6

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Wed, 05 Feb 2025 12:45:39 +0100

rust-sequoia-chameleon-gnupg (0.11.2-7) unstable; urgency=medium

  * Package sequoia-chameleon-gnupg 0.11.2 from crates.io using debcargo 2.7.5
  * d/rules:
    - on 64bit archs: also set opt-level="z" for further size reduction.

 -- Holger Levsen <holger@debian.org>  Fri, 20 Dec 2024 13:16:43 +0100

rust-sequoia-chameleon-gnupg (0.11.2-6) unstable; urgency=medium

  * Package sequoia-chameleon-gnupg 0.11.2 from crates.io using debcargo 2.7.4
    - refresh patch and relax dependency on sequoia-wot 0.13.

 -- Holger Levsen <holger@debian.org>  Sat, 30 Nov 2024 13:09:26 +0100

rust-sequoia-chameleon-gnupg (0.11.2-5) unstable; urgency=medium

  * Package sequoia-chameleon-gnupg 0.11.2 from crates.io using debcargo 2.7.4
  * d/rules:
    - on 64bit archs: build with lto="fat" and codegen-units=1, also see
      https://gitlab.com/sequoia-pgp/sequoia-chameleon-gnupg/-/issues/73.
    - on 32bit archs: disable lto optimisation.

 -- Holger Levsen <holger@debian.org>  Mon, 25 Nov 2024 20:29:54 +0100

rust-sequoia-chameleon-gnupg (0.11.2-4) unstable; urgency=medium

  * Package sequoia-chameleon-gnupg 0.11.2 from crates.io using debcargo 2.7.1
  * Drop patches/downgrade-indexmap-to-1.patch.
  * debian/patches: add dep3 headers.

 -- Holger Levsen <holger@debian.org>  Wed, 23 Oct 2024 13:06:31 +0200

rust-sequoia-chameleon-gnupg (0.11.2-3) unstable; urgency=medium

  * Package sequoia-chameleon-gnupg 0.11.2 from crates.io using debcargo 2.7.0
    - ship upstream manpages and shell completions.
    - update debian/patches/relax-deps.patch to allow -policy-config 0.7.

 -- Holger Levsen <holger@debian.org>  Fri, 11 Oct 2024 23:32:50 +0200

rust-sequoia-chameleon-gnupg (0.11.2-2) unstable; urgency=medium

  * Package sequoia-chameleon-gnupg 0.11.2 from crates.io using debcargo 2.7.0
    - bump build-depends to librust-base64-0.22+default-dev from 0.21.

 -- Holger Levsen <holger@debian.org>  Mon, 07 Oct 2024 13:56:03 +0200

rust-sequoia-chameleon-gnupg (0.11.2-1) unstable; urgency=medium

  * Package sequoia-chameleon-gnupg 0.11.2 from crates.io using debcargo 2.7.0
    - Bump standards version to 4.7.0, no changes needed.
    - Switch to debhelper-compat=13.
  * Update build-depends in d/control based on debcargo.hints.
  * Add myself to d/copyright.

 -- Holger Levsen <holger@debian.org>  Fri, 27 Sep 2024 22:20:02 +0200

rust-sequoia-chameleon-gnupg (0.10.1-2) unstable; urgency=medium

  * gpgv-sq: only suggest sq. Closes: #1077381.

 -- Holger Levsen <holger@debian.org>  Sun, 25 Aug 2024 14:47:17 +0200

rust-sequoia-chameleon-gnupg (0.10.1-1) unstable; urgency=medium

  * Package sequoia-chameleon-gnupg 0.10.1 from crates.io using debcargo 2.6.1
    Closes: #1073272, #1074652, #1070700.

 -- Holger Levsen <holger@debian.org>  Mon, 29 Jul 2024 16:39:06 +0900

rust-sequoia-chameleon-gnupg (0.9.0-1) unstable; urgency=medium

  * Package sequoia-chameleon-gnupg 0.9.0 from crates.io using debcargo 2.6.1
  * Make gpg-from-sq recommend gpgv-from-sq and make gpgv-from-sq suggest
    gpg-from-sq.

 -- Holger Levsen <holger@debian.org>  Tue, 11 Jun 2024 13:59:08 +0200

rust-sequoia-chameleon-gnupg (0.8.0-5) unstable; urgency=medium

  * gpg-from-sq.preinst and gpgv-from-sq.preinst: install diversion on upgrades
    as well. Closes: #1069741, #1069742.

 -- Holger Levsen <holger@debian.org>  Thu, 25 Apr 2024 19:31:12 +0200

rust-sequoia-chameleon-gnupg (0.8.0-4) unstable; urgency=medium

  * Fix ordering in gpg(v).links. Closes: #1069202.
  * Drop gpg(v)-from-sq.lintian-overrides.

 -- Holger Levsen <holger@debian.org>  Fri, 19 Apr 2024 08:46:04 +0200

rust-sequoia-chameleon-gnupg (0.8.0-3) unstable; urgency=medium

  * gpg-from-sq and gpgv-from-sq: ship links pointing to /usr/bin/gpg(v).
    Closes: #1069202.

 -- Holger Levsen <holger@debian.org>  Thu, 18 Apr 2024 18:16:15 +0200

rust-sequoia-chameleon-gnupg (0.8.0-2) unstable; urgency=medium

  * d/control:
    - add Breaks+Replaces to gpg-sq and gpgv-sq against sequoia-chameleon-gnupg
      (<< 0.8.0-1). Closes: #1069201.
    - add Provides: gpg (= 2.2.40) to gpg-from-sq. Closes: #1069205.
    - add Provides: gpgv (= 2.2.40) to gpgv-from-sq. Closes: #1069203.

 -- Holger Levsen <holger@debian.org>  Thu, 18 Apr 2024 16:37:51 +0200

rust-sequoia-chameleon-gnupg (0.8.0-1) unstable; urgency=medium

  * Package sequoia-chameleon-gnupg 0.8.0 from crates.io using debcargo 2.6.1

  [ Holger Levsen ]
  * Split sequoia-chameleon-gnupg binary package into five:
    - sequoia-chameleon-gnupg, which now just depends on gpgv-sq and gpg-sq.
    - gpgv-sq, shipping just that binary and it's manpage and various shell
      completion scripts.
    - gpg-sq, the same but for gpg-sq.
    - gpgv-from-sq: depends on gpgv-sq and installs a diversion to replace
      gpgv from g10code.
    - gpg-from-sq: the same but for gpg-sq.
  * Very thankfully merge Justin B Rye's superb suggestions from
    https://lists.debian.org/debian-l10n-english/2024/03/msg00017.html into
    d/control.
  * Make gpg-sq and gpgv-sq recommend sq.
  * Add lintian overrides for gpg-sq and gpgv-sq being empty packages (with
    maintainer scripts however).
  * Package sequoia-chameleon-gnupg 0.5.1 from crates.io using debcargo 2.6.1

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Tue, 16 Apr 2024 22:43:42 +0200

rust-sequoia-chameleon-gnupg (0.5.1-2) unstable; urgency=medium

  * Team upload.
  * Package sequoia-chameleon-gnupg 0.5.1 from crates.io using debcargo 2.6.1
  * Apply upstream patches for sequoia-cert-store 0.5 and sequoia-wot 0.10
  * Bump sequoia-ipc dependency to 0.34
  * Bump sequoia-wot dependency to 0.11
  * Stop patching sequoia-openpgp dependency
  * Adjust dependency on indexmap crate to prepare for rust-indexmap transition.

  [ Alexander Kjäll ]
  * Improve man page for gpg-sq.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 21 Mar 2024 20:43:53 +0000

rust-sequoia-chameleon-gnupg (0.5.1-1) unstable; urgency=medium

  * Package sequoia-chameleon-gnupg 0.5.1 from crates.io using debcargo 2.6.1
  * Add shell completions and man pages

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Thu, 22 Feb 2024 13:03:05 +0100

rust-sequoia-chameleon-gnupg (0.4.0-1) unstable; urgency=medium

  * Package sequoia-chameleon-gnupg 0.4.0 from crates.io using debcargo 2.6.1

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Wed, 31 Jan 2024 22:36:21 +0100
