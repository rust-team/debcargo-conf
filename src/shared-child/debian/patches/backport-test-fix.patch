From 80010627fa871cc9f469861a8c6675b6d6adc16a Mon Sep 17 00:00:00 2001
From: Jack O'Connor <oconnor663@gmail.com>
Date: Tue, 26 Sep 2023 16:36:37 -0700
Subject: [PATCH] stop asserting that child.kill() fails after wait

I've confirmed that this test still fails if we reap the child using
waitpid():

   -->8--
   │ diff --git a/src/lib.rs b/src/lib.rs
   │ index ca08e94..2b1479d 100644
   │ --- a/src/lib.rs
   │ +++ b/src/lib.rs
   │ @@ -171,7 +171,12 @@ impl SharedChild {
   │          // and signal the state condvar.
   │          let mut state = self.state_lock.lock().unwrap();
   │          // The child has already exited, so this wait should clean up without blocking.
   │ -        let final_result = noreap_result.and_then(|_| self.child.lock().unwrap().wait());
   │ +        let final_result = noreap_result.and_then(|_| unsafe {
   │ +            let mut status = 0;
   │ +            let pid = self.child.lock().unwrap().id() as libc::pid_t;
   │ +            libc::waitpid(pid, &mut status, 0);
   │ +            Ok(std::os::unix::process::ExitStatusExt::from_raw(status))
   │ +        });
   │          *state = if let Ok(exit_status) = final_result {
   │              Exited(exit_status)
   │          } else {
   -->8--

Fails with:

    ---- tests::test_into_inner_after_wait stdout ----
    thread 'tests::test_into_inner_after_wait' panicked at 'called `Result::unwrap()` on an `Err` value: Os { code: 10, kind: Uncategorized, message: "No child processes" }', src/lib.rs:410:22
---
 src/lib.rs | 10 ++--------
 1 file changed, 2 insertions(+), 8 deletions(-)

Index: shared-child/src/lib.rs
===================================================================
--- shared-child.orig/src/lib.rs
+++ shared-child/src/lib.rs
@@ -369,14 +369,8 @@ mod tests {
         shared_child.kill().unwrap();
         shared_child.wait().unwrap();
         let mut child = shared_child.into_inner();
-        // The child has already been waited on, so kill should be an error.
-        let kill_err = child.kill().unwrap_err();
-        if cfg!(windows) {
-            assert_eq!(std::io::ErrorKind::PermissionDenied, kill_err.kind());
-        } else {
-            assert_eq!(std::io::ErrorKind::InvalidInput, kill_err.kind());
-        }
-        // But wait should succeed.
+        // Wait should succeed. (Note that we also used to test that
+        // child.kill() failed here, but its behavior changed in Rust 1.72.)
         child.wait().unwrap();
     }
 
