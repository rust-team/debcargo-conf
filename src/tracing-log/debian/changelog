rust-tracing-log (0.2.0-3) unstable; urgency=medium

  * Team upload.
  * Package tracing-log 0.2.0 from crates.io using debcargo 2.7.4
  * Apply upstream fix for build failure in test, I suspect this
    build failure was triggered by a newer version of rustc but 
    I'm not sure.
  * Bump versions of ahash and criterion.
  * Stop marking tests as broken.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 25 Nov 2024 03:05:37 +0000

rust-tracing-log (0.2.0-2) unstable; urgency=medium

  * Team upload.
  * Package tracing-log 0.2.0 from crates.io using debcargo 2.6.1
  * Enable lru 0.10 patch.
  * Relax lru dependency.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 02 Jul 2024 20:32:24 +0000

rust-tracing-log (0.2.0-1) unstable; urgency=medium

  * Team upload.
  * Package tracing-log 0.2.0 from crates.io using debcargo 2.6.1
  * Add patch for lru 0.10 but don't enable it yet, it can be enabled when
    we are ready to perform the lru transition.
  * Stop marking all tests as broken.
  * Disable bench that requires a tracing-subscriber feature that is not
    currently available in Debian.
  * Set test_is_broken for the interest_cache feature, it's tests fail
    to build.
  * Fix tests with --no-default-features.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 06 Dec 2023 00:01:12 +0000

rust-tracing-log (0.1.3-2) unstable; urgency=medium

  * Package tracing-log 0.1.3 from crates.io using debcargo 2.5.0

 -- John Goerzen <jgoerzen@complete.org>  Mon, 31 Oct 2022 12:25:14 +0000

rust-tracing-log (0.1.3-1) unstable; urgency=medium

  * Package tracing-log 0.1.3 from crates.io using debcargo 2.5.0

 -- John Goerzen <jgoerzen@complete.org>  Thu, 20 Oct 2022 12:43:18 +0000
