rust-hyper (1.5.2-1) unstable; urgency=medium

  * Team upload.
  * Package hyper 1.5.2 from crates.io using debcargo 2.7.6

 -- Blair Noctis <ncts@debian.org>  Sun, 19 Jan 2025 06:13:18 +0000

rust-hyper (1.4.1-1~exp1) experimental; urgency=medium

  * Team upload.
  * Package hyper 1.4.1 from crates.io using debcargo 2.7.1
  * Disable unstable features: ffi, nightly, tracing; and dep on tracing
  * Properly gate tests
  * Drop outdated patches: relax-deps.patch, remove-missing-tests.patch,
  remove-pnet-datalink.patch, skip-checking-size-of-option-body.patch,
  remove-tokio-test.patch

 -- Blair Noctis <ncts@debian.org>  Sun, 03 Nov 2024 16:12:38 +0000

rust-hyper (0.14.27-2) unstable; urgency=medium

  * Team upload.
  * Package hyper 0.14.27 from crates.io using debcargo 2.6.1
  * Upgrade h2 dependency to 0.4

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Fri, 12 Apr 2024 09:18:30 -0400

rust-hyper (0.14.27-1) unstable; urgency=medium

  * Team upload.
  * Package hyper 0.14.27 from crates.io using debcargo 2.6.1 (Closes: #1055929)

 -- Peter Michael Green <plugwash@debian.org>  Tue, 28 Nov 2023 18:17:03 +0000

rust-hyper (0.14.25-2) unstable; urgency=medium

  * Team upload.
  * Package hyper 0.14.25 from crates.io using debcargo 2.6.0
  * Bump socket2 dependency to 0.5
  * Bump tokio dependency to 1.30 (the first version that uses socket2 0.5).

 -- Peter Michael Green <plugwash@debian.org>  Tue, 31 Oct 2023 17:53:06 +0000

rust-hyper (0.14.25-1) unstable; urgency=medium

  * Team upload.
  * Package hyper 0.14.25 from crates.io using debcargo 2.6.0 (Closes: #1043516)
  * Fix autopkgtest (it was previously skipped)
    + Remove refrences to tests/benches/examples that are not included in the
      crates.io release.
    + Remove dev-dependencies on on spmc, it does not seem to actually be
      needed.
    + Remove dev-dependencies on tokio-test and pnet-datalink and disable tests
      that require them the rest of the testsuite can run.
    + Set test_is_broken for the "ffi" feature, it refuses to build unless
      special flags are set.
    + Skip testing that Option<Body> and Body have the same size, it appears that
      the "niche" optimisations don't happen on at least i386.
    + Bump dev-dependency on pretty_env_logger

 -- Peter Michael Green <plugwash@debian.org>  Sun, 13 Aug 2023 02:51:44 +0000

rust-hyper (0.14.19-1) unstable; urgency=medium

  * Package hyper 0.14.19 from crates.io using debcargo 2.5.0
  * Set collapse_features = true

  [ Fabian Gruenbichler ]
  * Team upload.
  * Package hyper 0.14.18 from crates.io using debcargo 2.5.0

  * closes: #988729

 -- Peter Michael Green <plugwash@debian.org>  Fri, 24 Jun 2022 23:44:18 +0000

rust-hyper (0.12.35-1) unstable; urgency=medium

  * Package hyper 0.12.35 from crates.io using debcargo 2.2.10

  [ Ximin Luo ]
  * Team upload.
  * No need to exclude or clean Cargo.lock with dh-cargo >= 20.

 -- Andrej Shadura <andrewsh@debian.org>  Thu, 03 Oct 2019 15:09:28 +0200

rust-hyper (0.12.33-1) unstable; urgency=medium

  * Team upload.
  * Package hyper 0.12.33 from crates.io using debcargo 2.2.10

  [ kpcyrd ]
  * Package hyper 0.12.23 from crates.io using debcargo 2.2.10

 -- Andrej Shadura <andrewsh@debian.org>  Mon, 12 Aug 2019 18:15:44 +0200
