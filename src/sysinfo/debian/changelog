rust-sysinfo (0.30.13-7) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.30.13 from crates.io using debcargo 2.7.8
  * Add patch disabling MacOs-only crate

 -- Matthias Geiger <werdahias@debian.org>  Thu, 13 Mar 2025 19:17:26 +0100

rust-sysinfo (0.30.13-6) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.30.13 from crates.io using debcargo 2.6.1
  * Don't fail tests on riscv64 if CPU speed is unavailable.
  * Limit tests for cpu brand/vendor ID to x86* and arm*.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 03 Aug 2024 15:40:49 +0000

rust-sysinfo (0.30.13-5) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.30.13 from crates.io using debcargo 2.6.1
  * Extend dont-fail-tests-if-system-has-no-swap.patch

 -- Peter Michael Green <plugwash@debian.org>  Thu, 01 Aug 2024 14:22:25 +0000

rust-sysinfo (0.30.13-4) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.30.13 from crates.io using debcargo 2.6.1
  * Drop use-busy-wait-in-test-process-run-time.diff it didn't help
  * Mark test_process_run_time as broken on debci.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 31 Jul 2024 22:07:36 +0000

rust-sysinfo (0.30.13-3) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.30.13 from crates.io using debcargo 2.6.1
  * Skip test_components
  * Use a busy wait in test_process_cpu_time
  * Don't fail the tests if the system has no swap.
  * Add breaks on old version of rust-vergen.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 21 Jul 2024 13:20:15 +0000

rust-sysinfo (0.30.13-2) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.30.13 from crates.io using debcargo 2.6.1
  * Upload to unstable (Closes: #1075991)

 -- Peter Michael Green <plugwash@debian.org>  Sat, 20 Jul 2024 19:49:57 +0000

rust-sysinfo (0.30.13-1) experimental; urgency=medium

  * Team upload.
  * Package sysinfo 0.30.13 from crates.io using debcargo 2.6.1
  * Update patches for new upstream.
  * Remove windows-specific dependencies.
  * Don't fail tests if os has no version number (Debian testing/unstable don't)
  * Support specific target dir in tests.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 18 Jul 2024 21:26:53 +0000

rust-sysinfo (0.29.11-1) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.29.11 from crates.io using debcargo 2.6.1 (Closes: #1055096)
  * Update use-busy-wait-in-check-cpu-usage.diff for new upstream

 -- Peter Michael Green <plugwash@debian.org>  Sun, 07 Jul 2024 17:56:36 +0000

rust-sysinfo (0.28.4-4) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.28.4 from crates.io using debcargo 2.6.1
  * Skip test_physical_core_numbers on arm32
  * Skip test_refresh_tasks on all architectures.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 12 Dec 2023 23:56:51 +0000

rust-sysinfo (0.28.4-3) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.28.4 from crates.io using debcargo 2.6.1
  * Don't fail tests on arm32 if CPU speed is unavailable.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 12 Dec 2023 02:09:23 +0000

rust-sysinfo (0.28.4-2) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.28.4 from crates.io using debcargo 2.6.0
  * Use a busy wait instead of a sleep in check_cpu_usage to hopefully
    prompt some CPU usage.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 01 Jul 2023 00:31:42 +0000

rust-sysinfo (0.28.4-1) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.28.4 from crates.io using debcargo 2.6.0
  * Drop fix-race-in-test-wait-non-child.diff, a similar fix has been merged
    upstream.
  * Ignore newly introduced test check_all_process_uids_resolvable, there is
    no gaurantee that all uids are resolvabl on an arbitary test system.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 28 Jun 2023 21:50:06 +0000

rust-sysinfo (0.26.7-2) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.26.7 from crates.io using debcargo 2.6.0
  * Make test_wait_non_child more robust, hopefully fixing sporadic failures
    on arm*.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 17 Dec 2022 17:40:26 +0000

rust-sysinfo (0.26.7-1) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.26.7 from crates.io using debcargo 2.6.0 (Closes: #1024135)
  * Drop relax-deps.diff, no longer needed.
  * Drop fix-char-types.diff, fixed upstream.
  * Update ignore-tests-failing-in-build-environment.diff and
    ignore-tests-failing-debci for new upstream.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 20 Nov 2022 04:13:41 +0000

rust-sysinfo (0.23.13-2) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.23.13 from crates.io using debcargo 2.5.0

 -- Peter Michael Green <plugwash@debian.org>  Sun, 26 Jun 2022 03:34:17 +0000

rust-sysinfo (0.23.13-1) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.23.13 from crates.io using debcargo 2.5.0 (Closes: 1013137)
  * Fix/ignore tests that fail in chroot envrionments where no disks
    are visible.

  [ Wolfgang Silbermayr ]
  * Package sysinfo 0.22.5 from crates.io using debcargo 2.5.0
  * Add patch to ignore tests which wont't work in Debian's isolated build
    environment
  * Collapse features
  * Add patch to relax dependency versions

 -- Peter Michael Green <plugwash@debian.org>  Fri, 24 Jun 2022 02:07:15 +0000

rust-sysinfo (0.13.2-3) unstable; urgency=medium

  * Team upload.
  * Fix typo in debcargo.toml.

 -- Andrej Shadura <andrewsh@debian.org>  Tue, 14 Apr 2020 14:43:45 +0200

rust-sysinfo (0.13.2-2) unstable; urgency=medium

  * Team upload.
  * Ignore test failures.

 -- Andrej Shadura <andrewsh@debian.org>  Tue, 14 Apr 2020 11:02:43 +0200

rust-sysinfo (0.13.2-1) unstable; urgency=medium

  * Team upload.

  [ Andrej Shadura ]
  * Package sysinfo 0.13.2 from crates.io using debcargo 2.4.2.
  * Disable (by removing) the disk_list test.

  [ Sylvestre Ledru ]
  * Package sysinfo 0.12.0 from crates.io using debcargo 2.4.0.

 -- Andrej Shadura <andrewsh@debian.org>  Tue, 14 Apr 2020 10:28:25 +0200

rust-sysinfo (0.9.6-1) unstable; urgency=medium

  * Package sysinfo 0.9.6 from crates.io using debcargo 2.4.0

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Sun, 10 Nov 2019 13:55:06 +0100

rust-sysinfo (0.9.5-2) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.9.5 from crates.io using debcargo 2.2.10

  * Source upload for rebuild...

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 28 Oct 2019 21:02:08 +0100

rust-sysinfo (0.9.5-1) unstable; urgency=medium

  * Team upload.
  * Package sysinfo 0.9.5 from crates.io using debcargo 2.4.0

  [ Wolfgang Silbermayr ]
  * Package sysinfo 0.9.1 from crates.io using debcargo 2.4.0

 -- Sebastian Dröge <slomo@debian.org>  Fri, 18 Oct 2019 11:00:31 +0300

rust-sysinfo (0.8.0-1) unstable; urgency=medium

  * Package sysinfo 0.8.0 from crates.io using debcargo 2.2.9

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Wed, 26 Dec 2018 15:01:09 -0800
