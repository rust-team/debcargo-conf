rust-magic-wormhole (0.7.5-2) unstable; urgency=medium

  * Stop patching if-addrs 

 -- Matthias Geiger <werdahias@debian.org>  Thu, 06 Mar 2025 15:59:27 +0100

rust-magic-wormhole (0.7.5-1) unstable; urgency=medium

  * Package magic-wormhole 0.7.5 from crates.io using debcargo 2.7.6

 -- Matthias Geiger <werdahias@debian.org>  Mon, 27 Jan 2025 20:45:44 +0100

rust-magic-wormhole (0.7.4-2) unstable; urgency=medium

  * Upload to unstable 

 -- Matthias Geiger <werdahias@debian.org>  Wed, 04 Dec 2024 18:18:54 +0100

rust-magic-wormhole (0.7.4-1) experimental; urgency=medium

  * Package magic-wormhole 0.7.4 from crates.io using debcargo 2.7.5
  * Stop patching async-tungstenite
  * Refresh patches for new upstream release

 -- Matthias Geiger <werdahias@debian.org>  Tue, 03 Dec 2024 03:48:25 +0100

rust-magic-wormhole (0.7.3-3) unstable; urgency=medium

  * Stop patching zxcvbn

 -- Matthias Geiger <werdahias@debian.org>  Sun, 03 Nov 2024 15:42:53 +0100

rust-magic-wormhole (0.7.3-2) unstable; urgency=medium

  * Adjust zxcvbn patch

 -- Matthias Geiger <werdahias@debian.org>  Sun, 27 Oct 2024 16:34:27 +0100

rust-magic-wormhole (0.7.3-1) unstable; urgency=medium

  * Package magic-wormhole 0.7.3 from crates.io using debcargo 2.7.2
  * Refresh patches for new upstream
  * Drop d/p/drop-instant, upstreamed
  * Drop d/p/relax-env-logger, obsolete
  * Add patch to relax zxvcbn -dev-dependency
  * Add patch to to disable fzzt dependency; not yet packaged

 -- Matthias Geiger <werdahias@debian.org>  Sun, 27 Oct 2024 16:05:12 +0100

rust-magic-wormhole (0.7.0-2) unstable; urgency=medium

  * Drop base64 patch since 0.22 is now available 
  * Updated my mail address

 -- Matthias Geiger <werdahias@debian.org>  Sat, 05 Oct 2024 15:27:52 +0200

rust-magic-wormhole (0.7.0-1) unstable; urgency=medium

  * Package magic-wormhole 0.7.0 from crates.io using debcargo 2.6.1
  * d/patches/relax-if-addrs: refresh
  * Add patches to relax async-tungstenite, env-logger and base64 dependencies
  * Add patch to remove instant dependency; replace with
    std::time::Instant
  * Add patch to remove wasm dependencies
  * Add patch to remove async-native-tls feature
  * d/copyright: Add Fina Wilke as copyright holder/upstream contact

 -- Matthias Geiger <werdahias@riseup.net>  Thu, 18 Jul 2024 12:34:35 +0200

rust-magic-wormhole (0.6.1-3) unstable; urgency=medium

  * Skip send_many test always

 -- Matthias Geiger <werdahias@riseup.net>  Tue, 16 Jul 2024 17:27:04 +0200

rust-magic-wormhole (0.6.1-2) unstable; urgency=medium

  * Add patch to skip send_many test on amd64 and armel/hf

 -- Matthias Geiger <werdahias@riseup.net>  Mon, 15 Jul 2024 19:57:39 +0200

rust-magic-wormhole (0.6.1-1) unstable; urgency=medium

  * Package magic-wormhole 0.6.1 from crates.io using debcargo 2.6.1

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 07 Jul 2024 15:55:18 +0200
