rust-sequoia-wot (0.13.3-1) unstable; urgency=medium

  * Package sequoia-wot 0.13.3 from crates.io using debcargo 2.7.7
    - refresh patches.

 -- Holger Levsen <holger@debian.org>  Wed, 26 Feb 2025 17:34:22 +0100

rust-sequoia-wot (0.13.2-1) unstable; urgency=medium

  * Package sequoia-wot 0.13.2 from crates.io using debcargo 2.7.4

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 25 Nov 2024 20:10:39 -0500

rust-sequoia-wot (0.12.0-4) unstable; urgency=medium

  * Package sequoia-wot 0.12.0 from crates.io using debcargo 2.7.1
    - debian/patches: add dep3 headers.

 -- Holger Levsen <holger@debian.org>  Mon, 21 Oct 2024 12:39:01 +0200

rust-sequoia-wot (0.12.0-3) unstable; urgency=medium

  * Package sequoia-wot 0.12.0 from crates.io using debcargo 2.7.0
    - ship upstream manpages and shell completions.
    - add relax-deps.patch to allow build against newer sequoia-policy-config.

 -- Holger Levsen <holger@debian.org>  Fri, 11 Oct 2024 19:37:18 +0200

rust-sequoia-wot (0.12.0-2) unstable; urgency=medium

  * Team upload.
  * Package sequoia-wot 0.12.0 from crates.io using debcargo 2.6.1
  * Skip most tests on riscv64 due to timeouts.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 20 Aug 2024 17:49:37 +0000

rust-sequoia-wot (0.12.0-1) unstable; urgency=medium

  * Package sequoia-wot 0.12.0 from crates.io using debcargo 2.6.1
    Closes: #1074637.
  * Refresh patches.
  * Add myself to uploaders.

 -- Holger Levsen <holger@debian.org>  Tue, 02 Jul 2024 18:13:30 +0200

rust-sequoia-wot (0.11.0-1) unstable; urgency=medium

  * Package sequoia-wot 0.11.0 from crates.io using debcargo 2.6.1
  * Add tab completion setup for bash, fish and zsh.

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Thu, 14 Mar 2024 10:46:14 +0100

rust-sequoia-wot (0.9.0-1) unstable; urgency=medium

  * Team upload.
  * Package sequoia-wot 0.9.0 from crates.io using debcargo 2.6.1
  * Drop add-hash-bang.patch, upstream has added one.
  * Stop patching predicates dependency, what upstream wants is now in sync
    with what debian has.
  * Update enable-nettle.patch for new upstream.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 27 Jan 2024 16:22:19 +0000

rust-sequoia-wot (0.8.1-3) unstable; urgency=medium

  * Team upload.
  * Package sequoia-wot 0.8.1 from crates.io using debcargo 2.6.0
  * Change test dependencies to full-feature "gnupg" package rather than limited
    feature "gpg" package.
  * Try to improve error reporting in tests using gpg.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 09 Nov 2023 13:23:32 +0000

rust-sequoia-wot (0.8.1-2) unstable; urgency=medium

  * Team upload.
  * Package sequoia-wot 0.8.1 from crates.io using debcargo 2.6.0
  * Move test_depends to the correct section in debcargo.toml.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 09 Nov 2023 03:23:05 +0000

rust-sequoia-wot (0.8.1-1) unstable; urgency=medium

  * Team upload.
  * Package sequoia-wot 0.8.1 from crates.io using debcargo 2.6.0
  * Don't fail the build due to deprecated functions.
  * Bump predicates dev-dependency to 3.

  [ Alexander Kjäll ]
  * Package sequoia-wot 0.8.1 from crates.io using debcargo 2.6.0
  * Add myself as uploader.
  * Add hash-bang to gen-helper.sh
  * Remove windows-specific dependencies.
  * Add crypto-nettle feature to sequoia-openpgp dependency.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 07 Nov 2023 21:51:43 +0000

rust-sequoia-wot (0.2.0-1) unstable; urgency=medium

  * Package sequoia-wot 0.2.0 from crates.io using debcargo 2.5.0
    (Closes: #1011388)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 02 Jun 2022 11:50:44 +0200
