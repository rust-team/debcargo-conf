Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: sailfish-macros
Upstream-Contact: Ryohei Machida <orcinus4627@gmail.com>
Source: https://github.com/rust-sailfish/sailfish

Files: *
Copyright:
 2020-2023, Ryohei Machida "Kogia-sima" <orcinus4627@gmail.com>
 2022-2024, Vince Pike <vince@vinceworks.com>
 2024-2024, yaoxi-std <yaoxi20061225@126.com>
 2020-2020, Juan Aguilar Santillana  <mhpoin@gmail.com>
 2023-2023, Florian Will <florian.will@gmail.com>
 2023-2023, Daniel Arbuckle <djarb@highenergymagic.org>
 2020-2022, Sven <tomjordell@hotmail.com>
 2022-2022, Parampreet Rai <raja.rai6789@gmail.com>
 2023-2023, Lev Kokotov <lev.kokotov@gmail.com>
 2022-2022, Jérémie Drouet <jeremie.drouet@gmail.com>
 2024-2024, PaulDotSH
 2020-2020, <stoically@proprietary.lol>
 2023-2023, Newton Ni <nwtnni@gmail.com>
 2023-2023, Miran Bastaja <miran.bastaja04@gmail.com>
 2023-2023, liushuyu011@gmail.com <liushuyu011@gmail.com>
 2023-2023, Imbolc <me@imbolc.name>
 2024-2024, dianhenglau <dianhenglau@gmail.com>
 2024-2024, <ambisotopy@gmail.com>
License: MIT

Files: debian/*
Copyright:
 2024 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2024 Federico Ceratto <federico@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
